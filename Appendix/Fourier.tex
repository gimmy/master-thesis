\chapter{Fourier transform}\label{Appx:Fourier}
For the convenience of the reader, in this appendix we include some well known results about the Fourier transform. The interested reader can find the details in \cite{stein2011fourier}.

\paragraph{Riemann-Lebesgue}
\label{par:Riemann-Lebesgue}
The Fourier transform of a $L^1$ function is continuous and decays at infinity.
\begin{equation*}
  \Fourier \colon L^1(\R^d) \to C_0(\R^d) = \set{f \colon \R^d \to \R \text{ continuous, such that }  \lim_{\abs{\xi} \to \infty} \hat{f}(\xi) = 0 }.
\end{equation*}

\paragraph{Plancherel}
The Fourier transform $\Fourier$ is an isometry on $L^2$. More precisely
\begin{equation*}
  \Fourier \colon L^2(\R^d, \d x) \to L^2\pa{ \R^d, \frac{\d \xi}{(2\pi)^{\frac d2}} }.
\end{equation*}
One has
\begin{equation}\label{eq:Plancherel}%\tag{Plancherel}
    \norm{\,\cdot\,}_{L^2(\R^d)} = (2\pi)^{-\frac{d}{2}} \norm{ \Fourier[\,\cdot\,] }_{L^2(\R^d)} .
\end{equation}

\paragraph{Convolution}
On $\R^d$ the following identities hold
\begin{align}\label{eq:Convolution}
  \hat{f * g} = \hat{f} \cdot \hat{g} \qquad & \qquad \hat{f \cdot g} = \frac{1}{(2\pi)^d} \hat{f} * \hat{g} , \\
  \check{f \cdot g} = \check{f} * \check{g}  \qquad & \qquad  \check{f * g} = (2\pi)^d \check{f} \cdot \check{g} .
\end{align}

\paragraph{Inversion}
\begin{prop} Let $f,\hat{f} \in L^1(\R^d)$. % (and so in $L^1 \cap C_0$).
  Then
\begin{equation}\label{eq:Inversion}
  f(x) = \frac{1}{(2\pi)^d} \int_{\R^d} \hat{f}(\xi) e^{i x \cdot \xi} \d \xi . % \forall \, f \in \Schwartz(\R^d) .
\end{equation}
\end{prop}

\subsection*{Fourier transform of a measure}\label{sec:FTmeasure}
It is possible to define the Fourier transform of a \emph{finite} measure. The following is adapted from \cite[page 74]{tartar2007introduction}.\\

\noindent Let be $\mu$ a finite measure on $\R^d$. Consider the pairing with continuous compactly supported functions $\varphi \in C_c(\R^d)$ given by
% i.e. a Radon measure such that
\begin{equation*}
  \ang{\mu,\varphi} := \int_{\R^d} \varphi(x) \d \mu .
  \end{equation*}
  We have that
\begin{equation*}
   \abs{\ang{\mu,\varphi}} \leq C \norm{\varphi}_\infty \quad \text{ for all } \varphi \in C_c(\R^d).
\end{equation*}
If the above bound holds, the maps $\varphi \mapsto \ang{\mu,\varphi}$ extends in an unique way,
with the same bound,
to the Banach space $C_b(\R^d)$
of continuous bounded functions equipped with the sup norm $\norm{\,\cdot\,}_\infty$.
We can define the Fourier transform of such a measure as
\begin{equation*}
  \hat{\mu}(\xi) := \ang{\mu,e^{-i \ang{\cdot,\xi}}}.
\end{equation*}
By Dominated convergence theorem, $\hat{\mu}$ is continuous.

\subsection*{Fourier transform on tempered distributions $\Schwartz'(\R)$} \label{FTofDelta}
It is possible to apply the Fourier transform to a larger class objects. 
There are several books on the topic, for example the one by Strichartz \cite{strichartz2003guide}.

Consider a local integrable function $f$. We indicate with $T_f$ the corresponding distribution
in $\Schwartz'(\R)$. This is the tempered distribution such that, for every $\varphi \in \Schwartz(\R)$,
it is defined as
\begin{equation*}
  \ang{T_f, \varphi} := \int_{\R} f(x) \varphi(x) \d x .
\end{equation*}
Let $a \in \R$, and $\delta_a \in \Schwartz'(\R)$. Then, for every $\varphi \in \Schwartz(\R)$ we have
\begin{equation*}
  \ang{\hat{\delta_a}, \varphi} = \ang{\delta_a, \hat{\varphi}} = \hat{\varphi}(a)
  = \int e^{-i\ang{a, x}} \varphi(x) \d x = \ang{T_{e^{-i\ang{a,\cdot}}}, \varphi}
\end{equation*}
so
\begin{equation}
  \label{eq:FTofDelta}
  \hat{\delta_a} = e^{-i\ang{a,\,\cdot\,}}
\end{equation}
in distributional sense, meaning that
\begin{equation*}
  \int_{\set{\abs{x} < R}} e^{-i\ang{\cdot, x}} \d x \xrightarrow{R \to \infty} 2\pi~\delta_0
\end{equation*}
in the sense of tempered distributions
(i.e. in the weak-$\star$-topology on $\Schwartz'(\R)$).

\section{Littlewood-Paley theory}
\label{sec:LP}
We provide a short introduction to the Littlewood-Paley theory.
The interested reader can find an entire chapter on this topic in \cite[Chapter 5]{grafakos2008classical}.
\begin{lemma}
  There exists a smooth compactly supported function $\psi \in C^\infty_c(\R \setminus \set{0})$,
  non-negative, radial, such that
  \begin{equation*}
    \sum_{j \in \Z} \psi(2^{-j}x) = 1 , \quad \forall x \in \R \setminus \set{0}
  \end{equation*}
  and for any $x \neq 0$ the sum consists of at most two terms.
\end{lemma}
\begin{proof}
  Let $\chi \in C^\infty(\R)$ smooth, radial, non-negative, such that
  $\chi \equiv 1$ on the ball of radius $1$ and $\chi \equiv 0$ on the complement of the ball of radius $2$.
  Let $\psi(x) = \chi(x) - \chi(2x)$.
  For any $N \in \N$, the sum of
  the rescaled $\psi(2^{-j}x)$ is the telescopic sum:
  \begin{equation*}
    \sum_{j = -N}^N \psi_j(x) =  \sum_{j = -N}^N \bra{\chi(2^{-j}x) - \chi(2^{-j+1}x)} = \chi(2^{-N}x) - \chi(2^{N+1}x)
  \end{equation*}
  and for any $x \neq 0$ there exists $N \in \N$ such that
  $\chi(2^{-N}x) = 1$ and $\chi(2^{N+1}x) = 0$.
\end{proof}

\begin{define}[Littlewood-Paley projector]
  Let $\psi_j(x) := \psi(2^{-j}x)$. We define
  \begin{equation*}
    P_jf := (\psi_j \hat{f})^{\check{\;}}, \quad f \in \Schwartz(\R). 
  \end{equation*}
\end{define}

When $\xi \in \supp(\psi_j)$ then 
\begin{equation*}
  \xi \in A_j, \text{ where }   A_j = \set{\xi : 2^j \leq \abs{\xi} < 2^{j+1}}.
\end{equation*}
We indicate the condition $\xi \in A_j$ with $\abs{\xi} \simeq 2^j$. Thus
\begin{equation*}
  \hat{P_j f} = \1{A_j} \hat{f} .
\end{equation*}

\begin{define}[Littlewood-Paley square function]
  \begin{equation*}
    Sf(x) := \pa{ \sum_{j\in \Z} \abs{P_j f(x)}^2 }^{\frac12}.
  \end{equation*}
\end{define}

We have
\begin{equation}\label{eq:LP_SQf_bound}
  \norm{Sf}_{L^2(\R)}^2 = \int_{\R} \sum_{j \in \Z} \abs{P_j f}^2 = \sum_{j \in \Z} \int_{\R} \abs{P_j f}^2
  = \sum_{j \in \Z} \norm{P_j f}_{L^2(\R)}^2 \simeq \norm{f}_{L^2(\R)}^2 .
\end{equation}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../Thesis"
%%% End:
