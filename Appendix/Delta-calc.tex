%% \usepackage[sc]{mathpazo}
%% \linespread{1.05}         % Palatino needs more leading (space between lines)

% \usepackage{bookman}            % Bookman font
%% \usepackage{literat}

\chapter{An introduction to $\delta$-calculus}

Our aim in this Appendix is to give some formal derivation of $\delta$-calculus, which is largely used in this thesis. A quick introduction to the topic may be found in another Appendix in the survey by Foschi and Oliveira e Silva \cite{foschi2017some}.

Let $d, N \in \N$, with $N \geq d$, and let $\M$ a $d$-dimensional manifold in $\R^N$.
We would like to express the integral over the manifold $\M$ as integral over the whole Euclidean space, namely:
\begin{equation*}
  \int_\M \varphi(x) \d \sigma(x) = \int_{\R^N} F(\varphi,\mathbf{x}) \d \mu(\mathbf{x}) .
\end{equation*}

Above $\sigma$ is a measure on $\M$, it can be thought as the surface measure.
There are several ways to define it. The approach we present here uses the Hausdorff measure.

Recall that the $n$-dimensional Lebesgue measure $\Leb^n$ is defined as
\begin{equation*}
  \Leb^n(A) = \inf \set{ \sum_{i=1}^\infty \abs{B_i} \, : \, A \subset \bigcup_{i = 1}^\infty B_i } \qquad \text{ for any measurable set } A \subset \R^n.
\end{equation*}
The measure of a $n$-dimensional ball $\Ball^n$ with radius $r$ can also be written as
\begin{equation*}
  \abs{\Ball^n} = % check even/odd dimension  
\end{equation*}
We call such a  quantity $\alpha(n)$.

The diameter of a set is the ``maximal distance'' between two points in the set,
\begin{equation*}
  \diam(C) = \sup_{x, y \in C} \abs{x-y} .
\end{equation*}

\begin{define} The $k$-dimensional Hausdorff measure is defined as
  \begin{equation*}
  \H^k(A) = \inf \set{ \sum_{i=1}^\infty \pa{ \frac{\diam(C_i)}{2} }^k \, : \, A \subset \bigcup_{i = 1}^\infty C_i } \qquad \text{ for any measurable set } A \subset \R^n.
\end{equation*}
\end{define}

Intuitively, since the manifold has dimension smaller with respect the whole space,  
the measure $\mu$ will be singular respect the Lebesgue measure on the ambient space.

We start by recalling some well-known results that we will need later.
The interested reader can find all the proofs for example in \cite{evans2015measure}.



We start recalling the \emph{Coarea formula}. 
\begin{teo}
Let be $N,n \in \N$ with $N \ge n$,  $f \colon \R^N \to \R^n $ Lipschitz function. 
For every $\mathcal L^N$-measurable set $A \subset \R^N$ 
\[ \int_A \Jac_f (x) \d x = \int_{\R^n} \H^{N-n} (A \cap f^{-1}(y)) \d y \]
\end{teo}

By Rademacher's theorem, the Jacobian of a (locally) Lipschitz function exists almost everywhere  (respect to the Lebesgue measure).

\begin{ex}
Consider the ball $\Ball^3 \subset \R^3$ of radius $1$ and let be $f \colon \R^3 \to \R$ the projection on the last coordinate.
Then the Jacobian $\Jac_f \equiv 1$ and the preimages of $f$ intersected with $\Ball^3$ are plane disks. \\

\begin{minipage}{0,6\textwidth}
  Thus the Coarea formula tells us  that the volume of the ball can be calculated integrating 
  the $2$-dimensional Hausdorff measure of the disks $\Disk^2_y =  \Ball^3 \cap f^{-1}(y)$, %level sets of $f$ intersected with $\Ball^3$, 
  i.e. 
  \[ \int_{\Ball^3} \d x = \int_{\R} \H^{2} (\Ball^3 \cap f^{-1}(y)) \d y = \int_{-1}^1 \H^{2} (\Disk^2_y) \d y .\]
  Roughly speaking, ``summing all the parallel disks".
\end{minipage}
\begin{minipage}{0,4\textwidth}
\centering
  \begin{tikzpicture}
    %% \draw (-1,0) arc (180:360:1cm and 0.5cm);
    %% \draw[dashed] (-1,0) arc (180:0:1cm and 0.5cm);
    %\draw (0,1) arc (90:270:0.5cm and 1cm);
    %\draw[dashed] (0,1) arc (90:-90:0.5cm and 1cm);
    %\draw (0,0) circle (1cm);
    \shade[ball color=blue!10!white,opacity=0.40] (0,0) circle (1.5cm);
    \slice{60};
    \slice{30}; 
    \slice{0};
    \slice{-40};
    \node (punto) at (2.6,1)[label=below:$f^{-1}(y) \cap \Ball^3$] {};
    \node (punto) at (-1.4,-1)[label=below:$\Ball^3$] {};
\end{tikzpicture}
\vspace{2ex}
\end{minipage}\\
\end{ex}
%Of course the way of summing all these slices should take care of the way in which the slices are made. 
%The Jacobian in the formula takes care of this. In the case of real-valued function, like the projection above, this is just the norm of gradient.
%
%So, if we consider the double of the projection $g = 2 f$, then $\abs{\grad g} \equiv 2 $, we get:
%\[  \int_{\Ball^3} 2 \d x = \int_{\R} \H^{2} (\Ball^3 \cap g^{-1}(y)) \d y \] % \int_{} \H^{2} (\Disk^2_y) \d y \]

Another result following from the Coarea formula is the \emph{change of variables formula}.
\begin{teo}
Let be $f \colon \R^d \to \R$ Lipschitz function and
let be $g \in \L^1(\R^d)$ such that $g_{\restriction f^{-1}(y)} \in \L^1(f^{-1}(y), \H^{d-1})$, then

\begin{equation} \label{ChangeVariables} 
\int_{\R^d} g(x) \, \Jac_f(x) \d x = \int_{\R} \pa{\int_{f^{-1}(y)} g(x) \d \H^{d-1} } \d y .
\end{equation}
\end{teo}

As before, when the Jacobian $\Jac_f \equiv 1$, integrating $g$ over $\R^d$ is the same of integrating $g$ on the set $\{ f(x) = y \}$ and then integrating in $y$ over all level sets.
When $f$ is regular enough, the level sets of $f$ are (possibly degenerate) hypersurfaces. Let be $M_y = f^{-1}(y)$.

We want to obtain a formula for the integral of a function $\varphi$ over $M$, where  $M = M_0$. % $M = M_{y_0}$, for a fixed $y_0 \in \R$.
In other words, instead of integrating over all fibers of $f$, we want to integrate only over one preimage.
This would be the case if the Lebesgue measure $\d y$ was replaced by a Dirac measure $\delta_0$. 
For this purpose, consider a smooth, compactly supported function $\rho \in \Cs^\infty_c(\R^d)$  such that 
\[  \rho(x) \ge 0 \quad \forall x \in \R^d \, , \quad \int_{\R^d} \rho(x) \d x = 1 \, , \quad  \rho_\varepsilon(x) = \frac{\rho(x / \varepsilon) }{\varepsilon^{}} . \]

Assume that $\varphi \in \Cs^\infty_c(\R^d)$ and replace $g$ with the product $(\rho_{\varepsilon} \circ f) \, \varphi$ in the formua \eqref{ChangeVariables}. We have:
\[  \int_{\R^d} \rho_\varepsilon(f(x)) \, \varphi(x) \, \Jac_f(x) \d x = \int_{\R} \rho_\varepsilon(f(x)) \underbrace{ \pa{\int_{\{f(x) = y\} } \varphi(x) \d \H^{d-1} }}_{F(y)}  \d y = \int_{\R} \rho_\varepsilon(y) \, F(y) \d y . \]
When $\varepsilon$ goes to $0$, the function $\rho_\varepsilon$ converges to $\delta_0$ in distribution, then
\[  \int_{\R^d} \delta(f(x)) \, \varphi(x) \, \Jac_f(x) \d x = \int_{\R} \delta(y) \, F(y) \d y . \]

\paragraph{Note} In the previous limit we needed $F$ to be in $\Cs^\infty_c(\R)$. We took $\varphi \in \Cs^\infty_c(\R^d)$ to ensure this hyphotesis, but the conditions on $\varphi$ can be relaxed. %\TODO{Talk about this: for example if level sets of $f$ are compact}

Now the right hand side is equal to
\[  \ang{\delta}{F} = F(0) =  \int_{\{f(x) = 0\} } \varphi(x) \d \H^{d-1} . \]

In the case of a real-valued function $f$, like the projection above, the Jacobian $\Jac_f$ is just the norm of gradient $\abs{\grad f}$.
If this does not vanish, using \eqref{ChangeVariables} %\footnote{for example, when the matrix %representing the differential
%$D f$ has maximal rank in every point},
 we can write:
\[ \int_{\R^d} g(x) \d x = \int_{\R^d} g(x) \frac{\Jac_f(x)}{\abs{\grad f (x)}} \d x = \int_\R \pa{ \int_{\{f(x) = y\} } g(x)  \frac{\d \H^{d-1}}{\abs{\grad f (x)}} } \d y .\]
Thus, using $ \displaystyle g = \frac{\rho_\varepsilon(f)}{\abs{\grad f}} \varphi $ and taking the limit as $\varepsilon \to 0$ we obtain
\begin{equation}
\int_{\R^d} \delta(f(x)) \, \varphi(x) \d x =  \int_{M} \varphi(x) \frac{\d \H^{d-1}}{\abs{\grad f(x)}} .
\end{equation}

\subsection*{Product of delta}
Let consider $f, g \colon \R^d \to \R$ smooth functions. We indicate with $M_f$ and $M_g$ the hypersurfaces given by $f^{-1}(0)$ and $g^{-1}(0)$ respectively.
We can make sense of the integration of $\delta(f(x))$ over $M_g$ as integration of the product $\delta(f(x)) \cdot \delta(g(x))$ over $\R^d$, in fact:
\[ \int_{\R^d} \rho_\eta(g(x)) \delta(f(x)) \varphi(x) \Jac_g(x) \d x \overset{\text{Coarea}}{=} \int_\R \rho_\eta(t) \int_{g^{-1}(t)} \delta(f(x)) \varphi(x) \d \H^{d-1} \d t ,\]
taking the limit as $\eta$ goes to $0$ we obtain:
\[ \int_{\R^d} \delta(g(x)) \delta(f(x)) \varphi(x) \Jac_g(x) \d x = \int_{M_g} \delta(f(x)) \varphi(x) \d \H^{d-1} .\]

The integration of the product $\delta(f(x)) \delta(g(x))$ can be expressed as integration over the intersection $M_f \cap M_g$:
consider the function
\begin{align*}
  F(x) \colon & \R^d \to \R^2 \\
  & x \mapsto \pa{ f(x),g(x) }.
\end{align*}
Notice that $F^{-1}(y_1,y_2) = \{ x \in \R^d \colon f(x) = y_1 , g(x) = y_2 \} = \{ x \colon f(x) = y_1\} \cap \{ x \colon g(x) = y_2 \}$, for $y = (y_1,y_2) \in \R^2$.

Applying the Coarea formula to $F$ we obtain:
\[ \int_{\R^d} \psi(x) \, \Jac_F(x) \d x = \int_{\R^2} \pa{\int_{f^{-1}(y_1) \cap g^{-1}(y_2)}  \psi(x) \d \H^{d-2} } \d y. \]
Using $\displaystyle \psi(x) = \rho_\epsilon(f(x),g(x)) \, \varphi(x) \frac{\Jac_g(x)}{\Jac_F(x)}$  % note on directional bump functions

\begin{align*}
  \int_{\R^d} \rho_\epsilon(f(x),g(x)) \, \varphi(x) \Jac_g(x) \d x & = \int_{\R^2}  \rho_\epsilon(f(x),g(x)) \int_{f^{-1}(y_1) \cap g^{-1}(y_2)} \varphi(x) \frac{\Jac_g(x)}{\Jac_F(x)} \d \H^{d-2} \d y_1 \d y_2 \\
   \text{taking } \lim \text{ as } \epsilon \to 0 \quad & = \int_{M_f \cap M_g} \varphi(x) \frac{\Jac_g(x)}{\Jac_F(x)} \d \H^{d-2}
\end{align*}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../Thesis"
%%% End:
