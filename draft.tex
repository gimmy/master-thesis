\documentclass[11pt,reqno]{amsart}
%%%%%%%%%%%%%%%%%%%%%%
\headheight=8pt     \topmargin=0pt
\textheight=624pt   \textwidth=432pt
\oddsidemargin=18pt \evensidemargin=18pt
%%%%%%% Margins %%%%%%%%

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{fullpage}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{mathtools, dsfont, esint}

\usepackage{hyperref} % Required for adding links	and customizing them
% \definecolor{linkcolour}{rgb}{0,0,0} % {0,0.2,0.6} % Link color
% \hypersetup{colorlinks,breaklinks,urlcolor=linkcolour,linkcolor=linkcolour} % Set link colors throughout the document
\usepackage[capitalize]{cleveref}

\include{definitions}

% \usepackage{showkeys}
% \usepackage{todonotes}
\usepackage[disable]{todonotes}
% \usepackage[shortlabels]{enumitem}

\title{Extension to the 3-fold convolution \\  DRAFT {\footnotesize (\today)} }
\date{\today}

\begin{document}
\maketitle

We consider the following $1$-dimensional differential equation:
\[ \begin{dcases}
  i \partial_t u + \triangle^2 u = 0 \\
  u(0,x) = u_0(x) \in L^2(\R) .
\end{dcases} \]

The Strichartz estimate for the solution \cite{kenig1991oscillatory} states that
\begin{equation}
  \label{eq:Strichartz}
  \norm{D^{\frac13} e^{i t \triangle^2} f }_{L_{t,x}^6(\R \times \R)} \leq \mathbf{S} \Norm{f}_{L^2(\R)},
\end{equation}
where  $\mathbf{S}$ is the optimal constant.


In the spirit of \cite{quilodran2016extremizers}, we start by proving some properties of the convolution measure: % $w\nu_p * w\nu_p * w\nu_p$:
\begin{prop}[Analogous of Proposition 6.4, \cite{quilodran2016extremizers}]
  Given $p \geq 2$, let $w(\cdot) =\abs{\, \cdot\, }^{\frac{p-2}{3}}$. Let $\nu_p$ be the measure defined by
  \begin{equation*}
    \nu_p(\xi, \tau) = \delta(\tau - \abs{\xi}^p) \d \xi \d t .
  \end{equation*}
  The following assertions hold
  \begin{enumerate}%[(a)]
  \item[(a)] It is absolutely continuous with respect the Lebesgue measure on $\R^2$.
  \item[(b)] Its support is given by
    \[ E_p = \{ (\xi,\tau) \in \R^2 \, : \, \tau \geq 3^{1-p} \abs{\xi}^p \} .\]
  % \item Its Radon-Nikodym derivative, also denoted by $w\nu_p * w\nu_p * w\nu_p$, defines a bounded function in the interior of the set $E_p$. 
  \item[(c)] It is \emph{radial} in $\xi$, and homogeneous of degree zero, in the sense that:
    \[ (w\nu_p * w\nu_p * w\nu_p)(\lambda \xi,\lambda^p \tau) = (w\nu_p * w\nu_p * w\nu_p)(\xi,\tau), \quad \text{for every } \lambda > 0,\]
    and \, $(w\nu_p * w\nu_p * w\nu_p)(- \xi,\tau) = (w\nu_p * w\nu_p * w\nu_p)(\xi,\tau) \quad$ for every $\xi \in \R$.
  \end{enumerate}
\end{prop}

\begin{proof}
  As in Proposition 6.4 \todo{cite, and adapt}, with the change of variables: $\eta \mapsto \frac 23 \xi + \eta$ , $\zeta \mapsto\frac{\xi}{3} - \zeta$,
  we can write the convolution measure as
  \begin{align*}
    \label{eq:convolution}
    \hspace{-0.5cm}   (w\nu_p * w\nu_p * w\nu_p)(\xi,\tau) & = \iint_{\R^2} {\textstyle \delta(\tau - \abs{\frac{\xi}{3}- \eta}^p - \abs{\frac{\xi}{3}- \zeta}^p - \abs{\frac{\xi}{3}+\eta+\zeta}^p) \left(\abs{\frac{\xi}{3}- \eta}  \, \abs{\frac{\xi}{3}- \zeta} \, \abs{\frac{\xi}{3}+\eta+\zeta}\right)^{\frac{p-2}{3}} } \d\eta \d\zeta \\
    \hspace{-0.5cm} & = \iint_{\R_\eta \times \R_\zeta} A_{\xi,\tau}(\eta,\zeta) \d\eta \d\zeta .
  \end{align*}    
  \begin{itemize}
  \item[(a),(c)] It is enough to change variables and use that $\delta(\lambda^p F(x) ) = \lambda^{-p}\delta(F(x))$ \todo{cite Diogo \& Foschi: Recent progress}.
    Note also that
    \begin{equation*}
      \hspace{-0.5cm}   (w\nu_p * w\nu_p * w\nu_p)(-\xi,\tau) = \iint A_{-\xi,\tau}(\eta,\zeta) \d\eta \d\zeta =  \iint A_{\xi,\tau}(-\eta,-\zeta) \d\eta \d\zeta = (w\nu_p * w\nu_p * w\nu_p)(\xi,\tau).
    \end{equation*}
  \item[(b)] We first we show that every point in $E_p$ belongs to the support of $w\nu_p * w\nu_p * w\nu_p$.
    In fact, let be $\psi(\cdot)=\abs{\, \cdot \,}^p$ consider $y_1, y_2, y_3 \in \R$ such that
    \[ \xi = y_1+y_2+y_3, \quad  \tau = \psi(y_1) + \psi(y_2) + \psi(y_3) ,\]
    then, from the midpoint convexity of $\psi$ it follows:
    \begin{equation}
      \label{eq:convexity}
      \frac 13 \tau = \frac 13 \psi(y_1) + \frac 13 \psi(y_2) + \frac 13 \psi(y_3) \geq \psi\left( \frac{y_1 + y_2 + y_3}{3} \right) = \psi(\xi/3).
    \end{equation}
    On the other hand, consider $(\xi,\tau) \in \R^2$ such that $\tau \geq 3 \psi(\xi/3)$. We want to find $y_1, y_2, y_3 \in \R$ as before.
    It is enough to find $y_1, y_2$, since $y_3 = \xi - (y_1 + y_2)$. The left hand side of \cref{eq:convexity} is convex and continuous, so it goes to infinity as $\abs{(y_1,y_2)} \to \infty$. Then, for every fixed $\tau \geq 3 \psi(\xi/3)$, for the Intermediate Values Theorem, exist $(y_1,y_2) \in \R^2$ such that
    \begin{equation*}
      \tau = \psi(y_1) + \psi(y_2) + \psi(\xi - (y_1+y_2)) \geq 3 \psi(\xi/3).
    \end{equation*}

    \iffalse
  \item[(c)] Because of (d), we can assume $\xi = 3$, then $\tau = 3^p \lambda$ and we can rewrite the convolution measure %\eqref{eq:convolution}
    as    
    \begin{equation*}
      \hspace{-1.5cm}   (w\nu_p * w\nu_p * w\nu_p)(3,3^p \lambda) = \iint_{\R^2} {\textstyle \delta(3^p \lambda - \abs{1- \eta}^p - \abs{1-\zeta}^p - \abs{1+\eta+\zeta}^p) (\abs{1- \eta}^2 \, \abs{1-\zeta}^2 \, \abs{1+\eta+\zeta}^2)^{\frac{p-2}{6}} } \d \eta \d \zeta .
    \end{equation*}
    Consider $v := (\eta,\zeta)$ as a vector in $\R^2$, we can use polar coordinate % such that    
    % \[
    %   \ang{v,(\sgn(\xi),0)} = \eta, \quad \text{ and so let be } \quad      
    %   \begin{dcases}
    %     \eta  & = r \cos \theta \\
    %     \zeta & = r \sin \theta .
    %   \end{dcases} \]
    we get
    \begin{equation}
      \label{eq:polar}
      (w\nu_p * w\nu_p * w\nu_p)(3,3^p \lambda) = \int_0^{2\pi} \int_{0}^\infty {\textstyle \delta(3^p \lambda - 3 - \varphi_{\theta}(r) ) } \, \mathds W(\theta, r) \d r \d \theta .
    \end{equation}
    The function $\varphi_\theta(r)$ given by
    \begin{equation*}
      \varphi_\theta (r) = \pa{(r\cos\theta -1)^2}^{\frac p2} + \pa{(r\sin\theta-1)^2}^{\frac p2} + \pa{(1+ r(\cos\theta+\sin\theta))^2}^{\frac p2} -3
    \end{equation*}
    % \begin{eqnarray*}
    %   \varphi_\theta (r) & = & (1- 2r \cos \theta + r^2 \cos^2 \theta)^{\frac p2} + (1-2r \sin\theta + r^2 \sin^2 \theta)^{\frac p2} \\
    %                     & + & (1+ 2r(\cos\theta+\sin\theta) + r^2(\sin\theta + \cos\theta)^2)^{\frac p2} -3
    % \end{eqnarray*}
    is convex, because sum of convex functions, with unique global minimum at $r=0$.
    While the product of weights $\mathds W(\theta,r)$ is given by
    \begin{equation*}
      \mathds W(\theta, r) = \pa{ (r \cos \theta -1)^2 (r \sin\theta -1)^2 (1+r\cos\theta+r\sin\theta)^2 }^{\frac{p-2}{6}} r .
    \end{equation*}
    The derivative $\varphi_\theta'(r)$ is given by
%    \begin{eqnarray*}
%      \varphi_\theta'(r) =&  & p \cos \theta (r \cos \theta -1)(1-2r\cos\theta+r^2 \cos^2\theta)^{\frac{p-2}{2}} \\ 
%                          &  +  & p \sin\theta (r \sin\theta -1)(1-2r\sin\theta + r^2 \sin^2\theta)^{\frac{p-2}{2}} \\
%                          &  +  & p (r + \sin\theta + \cos\theta) (1+2r(\sin\theta + \cos\theta) + r^2)^{\frac{p-2}{2}}.
%    \end{eqnarray*}
%    We can collect some term obtaining:
    \begin{eqnarray*}
      \varphi_\theta'(r) & = & p \cos \theta \pa{(r \cos \theta -1)^2}^{\frac p2} + p \sin\theta \pa{(r \sin\theta -1)^2}^{\frac p2} \\
                          &  +  & p (\sin\theta + \cos\theta) \pa{(1 + r\sin \theta + r\cos \theta)^2}^{\frac{p}{2}} .
    \end{eqnarray*}
    % Notice that when $r=0$ we have
    % \begin{equation*}
    %   \varphi_\theta'(0) =  p (\cos\theta +\sin\theta)(-1)^{p-1} + p (\sin\theta + \cos\theta) = 0 \quad \text{ for even } p.
    % \end{equation*}

    % TODO: check if this is the only zero of $\varphi_\theta'$ in this case. (Numerically checked for $p=4$).

    The change of variables $s = \varphi_\theta(r)$ yields
    \begin{align*}    
      (w\nu_p * w\nu_p * w\nu_p)(3,3^p \lambda) = & \int_0^{2\pi} \int_{0}^\infty \delta(3^p \lambda - 3 - s ) \, \frac{\mathds{W}(\theta, r)}{\varphi_\theta'(r)} \d s \d \theta \\
      =  & \, \1{\set{\lambda \geq 3^{1-p}}}(\lambda) \int_0^{2\pi} \frac{\mathds{W}(\theta, r)}{\varphi_\theta'(r)} \d \theta.
    \end{align*}
    where $r = \varphi_\theta^{-1}(3^p\lambda - 3)$. Since $\varphi_\theta$ is non-negative, convex and continuous, the inverse map
    \begin{equation*}
      \varphi_\theta^{-1} \colon \R^+ \to \R^+
    \end{equation*}
    is well-defined. (to finish)

    \fi
    
  \end{itemize}

\end{proof}


\newpage
\section{Lower bound for $\mathbf S$}

\subsection{A first attempt}

We start proving a result analogous to the \cite[Lemma~6.1]{quilodran2016extremizers} adapted for the $3$-fold convolution where the perturbation is $\Psi(x) = x^4$.
\begin{lemma} \label{lm:sloppy_lower_bound}
  Consider the measure $\nu(y,t) = \ddelta{t-y^4} \d y \d t$. Let $E$ denote the support of the convolution measure $\nu*\nu*\nu$.
  Let be $w(\cdot)$ the non-negative function $\abs{\,\cdot\,}^{\frac23}$ and consider $f(x) = e^{-x^4}\sqrt{w(x)} \in L^2(\R)$, then $f \sqrt{w} \, \nu * f \sqrt{w} \, \nu * f \sqrt{w} \, \nu  \in L^2(\R^2)$ and the following lower bound holds:
  \begin{equation}
    \label{eq:sloppy_lower_bound}
    \frac{\norm{f \sqrt{w} \, \nu * f \sqrt{w} \, \nu * f \sqrt{w} \, \nu }^2_{L^2(\R^2)} }{\norm{f}^6_{L^2(\R)}} \geq \frac{ \norm{f}^6_{L^2(\R)} }{\int_E e^{-2\tau} \d\tau \d\xi}.
  \end{equation}
\end{lemma}

\begin{proof}
  The following hold:
  \begin{eqnarray}
  ( f \sqrt w \nu * f \sqrt w \nu * f \sqrt w \nu)(\xi,\tau) = e^{-\tau} (w \nu * w \nu * w \nu)(\xi,\tau),     \label{eq:identities_for_f_1} \\
    ( f^2 \nu * f^2 \nu * f^2 \nu)(\xi,\tau) = e^{-\tau} ( f \sqrt w \nu * f \sqrt w \nu * f \sqrt w \nu)(\xi,\tau).     \label{eq:identities_for_f_2}
\end{eqnarray}
Moreover we have
\begin{equation}
  \label{eq:convolution_is_actually_norm}
  \int_{\R^2}( f^2 \nu * f^2 \nu * f^2 \nu)(\xi,\tau) \d\xi\d\tau = \norm{f}_{L^2(\R)}^6.
\end{equation}
Then using the preceding identities and Cauchy-Schwarz we obtain
\begin{align*}
  \norm{f}_{L^2(\R)}^6 & = \int_{\R^2} e^{-\tau} ( f \sqrt w \nu * f \sqrt w \nu * f \sqrt w \nu)(\xi,\tau) \d\tau \d\xi \\
  & \leq \pa{\int_{E} e^{-2\tau} \d\tau\d\xi}^{\frac12} \, \norm{f \sqrt w \nu * f \sqrt w \nu * f \sqrt w \nu}_{L^2(\R^2)}.
\end{align*}
that implies the desired inequality \eqref{eq:sloppy_lower_bound}.
\end{proof}


We want to show a lower bound for the constant
\begin{equation}
  \label{eq:sharp_constant}
  \mathbf{S} := \sup_{\substack{f \in L^2(\R), \\ f \neq 0}} \frac{ 6^{\frac16} \, \norm{\nabla^{\frac13} e^{i t \triangle^2} f}_{L_{t,x}^6(\R \times \R)}}{\Norm{f}_{L^2(\R)}}
\end{equation}
or, in convolution form:
\begin{equation}
  \label{eq:sharp_constant_conv}
{\color{red} 2\pi\,} \mathbf{S}^6 = \sup_{\substack{f \in L^2(\R), \\ f \neq 0}} \frac{6 \, \norm{f \sqrt w \nu * f \sqrt w \nu * f \sqrt w \nu}^2_{L^2(\R^2)} }{\norm{f}^6_{L^2(\R)}}.
\end{equation}

Consider the sharp constant for the Strichartz estimate for the Schr\"odinger equation:
\begin{equation*}
  \Schr := \sup_{v \in L^2(\R) \setminus \{ 0 \} } \frac{\norm{e^{-it\triangle}v}_{L_{t,x}^6(\R \times \R)} }{\norm{v}_{L^2(\R)}} = \pa{\frac{1}{12}}^{\frac{1}{12}} % \sqrt[6]{\frac{\pi}{6\sqrt 3}} \approx 0.81923\dots.
\end{equation*}

In \cite[Theorem~1.8]{jiang2010linear} the authors proved a dichotomy result for extremizers of \eqref{eq:sharp_constant}:
\begin{teo}[Dichotomy]
  Either
  \begin{itemize}
  \item[(i)] $\mathbf{S} = \Schr$ and there exists $f \in L^2(\R)$ and a sequence $\{a_n\}_{n \in \N}$ going to infinity as $n \to \infty$, such that $\{e^{ixa_n}f\}_{n \in \N}$ is an extremizing sequence for \eqref{eq:sharp_constant}, \emph{or}
  \item[(ii)] $\mathbf{S} > \Schr$ and extremizers for \eqref{eq:sharp_constant} exist. \label{th:to_show}
  \end{itemize}
\end{teo}
Our goal is to prove a lower bound for $\mathbf{S}$ good enough to rule out the first case.
For this we need to approximate the value of $\mathbf{S}$ for some explicit function $f$.

\begin{remark}
  Notice that $\mathbf{S} \geq \Schr$. To show the second point of \cref{th:to_show} it is enough to exhibit a function $f$ such that
  \begin{equation*}
    \frac{\norm{f \sqrt w \nu * f \sqrt w \nu * f \sqrt w \nu}^2_{L^2(\R^2)} }{\norm{f}^6_{L^2(\R)}} > {\color{blue} \frac{\pi}{{\color{red}6}\sqrt{3}} }
  \end{equation*}
  where $\frac{\pi}{\sqrt{3}}$ is $\Schr$ in convolution form squared, and the number $6$ comes from \eqref{eq:sharp_constant_conv}.
\end{remark}

We can use $f$ as in the \cref{lm:sloppy_lower_bound} and calculate the right hand side of \eqref{eq:sloppy_lower_bound}:
\begin{equation} \label{eq:first_approximation}
  \frac{ \norm{f}^6_{L^2(\R)} }{\int_E e^{-2\tau} \d\tau \d\xi} = 
  \frac{1}{2^{4}3^{\frac34}} \, \frac{\Gamma(\frac{5}{12})^3}{\Gamma(\frac54)} \approx 0.2913141\dots
\end{equation}
In fact, since $f$ is an even function we have
\begin{equation*}
  \norm{f}_{L^2(\R)}^2 = \int_{\R} e^{-2x^4} (x^2)^{\frac13} \d x = \frac12 \int_0^\infty e^{-2z} z^{\frac{5}{12} -1} \d z = \frac12 \frac{\Gamma(\frac{5}{12})}{2^{\frac{5}{12}}}, \qquad \norm{f}_{L^2}^6 = \frac{1}{2^4} \frac{\Gamma(\frac{5}{12})^3}{2^\frac14}
\end{equation*}
Recalling that the support of $\nu*\nu*\nu$ is $E = \set{(\xi,\tau) \in \R^2 : \tau \geq \frac{\xi^4}{27}}$, we compute the denominator:
\begin{equation*}
  \displaystyle  \int_E e^{-2\tau} \d\tau\d\xi = \int_\R \int_{\frac{1}{27}}^\infty e^{-2 \lambda \xi^4} \xi^4 \d\lambda\d\xi = \frac12 \int_{\frac{1}{27}}^\infty \pa{ \int_0^\infty e^{-2u} u^{\frac54 -1} \d u} \frac{\d\lambda}{\lambda^{\frac14 + 1}} 
  = \frac{3^{\frac34} \, \Gamma(\frac54)}{2^{\frac14}}.
\end{equation*}
\\
Unfortunately, the optimal constant is still larger\footnote{In fact $\frac{\pi}{6\sqrt3} \approx 0.302299 > 0.2913141$} than the lower bound above.

\subsection{Improving lower bound}
The inequality in \cref{lm:sloppy_lower_bound} was too sloppy and we need a new way to approximate $\norm{f \sqrt w \nu * f \sqrt w \nu * f \sqrt w \nu}^2_{L^2(\R^2)}$.
For this purpose, we exploit the identities \eqref{eq:identities_for_f_1}, \eqref{eq:identities_for_f_2}, and the following property
of the convolution measure (as in \cite[Proposition~6.4]{quilodran2016extremizers}):
\begin{prop}
  Let be $w(\cdot) =\abs{\, \cdot\, }^{\frac{2}{3}}$ and $\nu$ be the measure defined by
  \begin{equation*}
    \nu(\xi, \tau) = \ddelta{\tau - \abs{\xi}^4} \d\xi \d\tau .
  \end{equation*}
  Then the following properties hold for the convolution measure $w\nu *w\nu *w\nu$.
  \begin{enumerate}%[(a)]
  \item[(a)] It is absolutely continuous with respect the Lebesgue measure on $\R^2$.
  \item[(b)] Its support is given by
    \[ E = \{ (\xi,\tau) \in \R^2 \, : \, \tau \geq 3^{-3} \abs{\xi}^4 \} .\]
    % \item Its Radon-Nikodym derivative, also denoted by $w\nu_p * w\nu_p * w\nu_p$, defines a bounded function in the interior of the set $E_p$. 
  \item[(c)] It is \emph{radial} and \emph{homogeneous} of degree zero in $\xi$, the sense that:
    \begin{equation*}
      (w\nu *w\nu *w\nu)(\lambda \xi, \lambda^4 \tau) =  (w\nu *w\nu *w\nu)(\xi,\tau), \quad \text{ for every } \lambda > 0.
    \end{equation*}
    and \, $(w\nu * w\nu * w\nu)(- \xi,\tau) = (w\nu * w\nu * w\nu)(\xi,\tau) \quad$ for every $\xi \in \R$.
  \end{enumerate}
\end{prop}
We postpone the proof. \todo{insert the proof}

The quantity we want to compute equals
\begin{align*}
  \int_{\R^2} e^{-2\tau} (w\nu *w\nu *w\nu)^2(\xi,\tau) \d\xi \d\tau & = \int_{\frac{1}{27}}^\infty (w\nu *w\nu *w\nu)^2(1,\lambda) \, 2 \int_0^\infty e^{-u} \pa{\frac{u}{\lambda}}^{\frac54 -1} \frac{\d u}{4 \lambda} \, \d\lambda \\
  & = \frac{\Gamma\pa{\frac54}}{2^\frac14} \int_0^{3^{\frac34}} (w\nu *w\nu *w\nu)^2(1,t^{-4}) \d t .
\end{align*}

After rescaling, since the integrand is even, we can write
\begin{equation*}
  \int_0^{3^{\frac34}} (w\nu *w\nu *w\nu)^2(1,t^{-4}) \d t = 3^\frac34 \cdot \frac12 \int_{-1}^1 g^2(t) \d t
\end{equation*}
where the function of the right hand side is $g(t) = (w\nu *w\nu *w\nu)(1,3^{-3}t^{-4})$. We have obtained
\begin{equation} \label{eq:quantity_with_coefficients}
  \frac{\norm{f \sqrt w \nu * f \sqrt w \nu * f \sqrt w \nu}^2_{L^2(\R^2)}}{\norm{f}_{L^2(\R)}^6} = \pa{\frac{2^4 3^\frac34  \Gamma\pa{\frac54}}{\Gamma\pa{\frac{5}{12}}^3}} \, \fint_{-1}^1 g^2(t) \d t .
\end{equation}
Thus, the problem of computing a lower bound for the numerator in \eqref{eq:sharp_constant_conv} reduce to approximate the norm of $g$ in $L^2([-1,1])$ equipped with the normalized scalar product $\nang{f,g} = \fint_{-1}^1 f g$.
With this purpose in mind, consider the set of all monomials $\set{1,t,t^2,\dots}$. It is a complete system in $L^2([-1,1])$. Using Gram-Schmidt we obtain a well-known orthogonal basis.
\begin{define}[Legendre polynomials]
  We denote with $P_n = P_n(t)$ the $n^{\text{th}}$ Legendre polynomial.
  Then $\set{P_n}_{n \in \N}$ is an orthogonal basis of $L^2([-1,1])$; the first five elements are
    \begin{align*}
    P_0(t) &= 1 , \\
    P_1(t) &= t, \\
    P_2(t) &= \textstyle \frac{1}{2} \, (3 t^2-1), \\
    P_3(t) &= \textstyle \frac{1}{2} \, (5 t^3-3 t) , \\
    P_4(t) &=  \textstyle \frac18 \, (35 t^4 - 30 t^2 +3). 
    \end{align*}
    Moreover, with the normalized scalar product: $\nnorm{P_n}^2 = \frac{1}{2n+1}$.
    We indicate with $Q_n = \frac{P_n}{\nnorm{P_n}}$. Then $\set{Q_n}_{n \in \N}$ is an orthonormal basis of $L^2([-1,1], \nang{\cdot,\cdot})$.
\end{define}

\begin{remark}
  The norm of $g$ can be written as
  \begin{equation*}
    \nnorm{g}_{L^2}^2 = \sum_{n \geq 0} \nang{g,Q_n}^2 = \sum_{n \geq 0} \nang{g,Q_{2n}}^2, % = \sum_{n \geq 0} (c_{2n})^2 ,
  \end{equation*}
  since the function $g$ is even. We can approximate the norm calculating arbitrarily many coefficients:
  \begin{equation*}
    c_n^2 :=  \nang{g,Q_n}^2  =  (2n+1) \, \pa{\fint_{-1}^1 g(t) P_n(t) \d t}^2 .
  \end{equation*}
\end{remark}
These coefficients contains the same information of the \emph{moments} of the measure $g$:
\begin{equation*}
 \mathcal I_k := \int_{-1}^1 g(t) \, t^k \d t, 
\end{equation*}
once we have them, one can immediately obtain the value of $c_n$.

\subsection{Computing moments}
To compute $\mathcal I_k$ we consider a new function $h_b$ such that $h_b^2(x) = f(x) \sqrt{w(x)} e^{bx}$.
In view of \eqref{eq:identities_for_f_2} and \eqref{eq:convolution_is_actually_norm} we have
\begin{align*}
  \int_{\R^2}( h_b^2 \nu * h_b^2 \nu * h_b^2 \nu)(\xi,\tau) \d\xi\d\tau & = \pa{\norm{h_b}_{L^2(\R)}^2}^3 =: G(b)^3 \\
  \int_{\R^2}( h_b^2 \nu * h_b^2 \nu * h_b^2 \nu)(\xi,\tau) \d\xi\d\tau & =   \int_{\R^2} e^{-(\tau-b\xi)}( w \nu * w \nu * w \nu)(\xi,\tau) \d\xi\d\tau =: F(b).
\end{align*}
We expand $F$ and $G$ as Taylor series. After rearranging coefficients we have:
\begin{align*}
  F(b) & = \sum_{n \geq 0} \frac{F^{(n)}(0)}{n!} \, b^n \\
  G(b)^3 & =  \pa{\sum_{n \geq 0} \frac{G^{(n)}(0)}{n!} \, b^n }^3 =  \sum_{n \geq 0} \frac{d_n}{n!} \, b^n .
\end{align*}
% where $d_n$ is given by
% \begin{equation*}
%   d_n = \sum_{m=0}^n \pvector{n \\ m} \frac{1}{m!} \sum_{k=0}^m \pvector{m \\ k} \pa{ G^{(k)} \cdot G^{(m-k)} \cdot G^{(n-m)}}(0) .
% \end{equation*}

\begin{remark}
  The functions $F$ and $G$ are \emph{even}, in particular in the above series only even coefficients appear.
  The first five coefficients of the second expansion are
  \begin{align*}
    d_0 &= G(0)^3 \\
    d_2 &= 3 \, G(0)^2 G^{''}(0)  \\
    d_4 &= \textstyle 3 \, G(0) \pa{ 6 \, G^{''}(0)^2 + G(0) G^{(4)}(0) }
  \end{align*}
  and $d_{2n+1} = 0$ for every $n \geq 0$.
\end{remark}

We compute the derivatives:
\begin{eqnarray*}
  F^{(2k)}(0) = & (3^{\frac34})^{2k+1} \, \Gamma(\frac54 + \frac k2) \, \mathcal{I}_{2k}, \quad \text{ and } \\ % 2 simplify with the 1/2 in the integral
  G^{(2k)}(0) = &  2^{-1} \, \Gamma\pa{\frac{5}{12}+\frac k2} .
\end{eqnarray*}

Since the expressions for $F^{(n)}(0)$ encapsulate the one for the moments,
we can compute them comparing with the explicit values of $d_n$:
\begin{equation*}
  \mathcal{I}_{2n} = \frac{d_{2n}}{(3^{\frac34})^{2n+1} \Gamma\pa{\frac54 + \frac n2}}.
\end{equation*}

The double of the first coefficient $c_0$ equals the first moment $\mathcal I_0$, so, using the formulas above, it's enough to compare the first coefficients of the two series:
\begin{equation*}
\begin{cases}
  F(0) & = 3^{\frac34} \Gamma(\frac54) \, 2 \, c_0  \\
  G(0)^3 & = 2^{-3} \pa{\Gamma(\frac{5}{12})}^3
\end{cases}
\quad \then \quad  c_0 = \frac{\pa{\Gamma(\frac{5}{12})}^3}{2^4 3^{\frac34}\Gamma(\frac54)} .
\end{equation*}

\begin{remark}
  This quantity is closely related to the constant in front of the squared norm of $g$ in \eqref{eq:quantity_with_coefficients}: in fact, that constant is exactly $c_0^{-1}$.
  So, if we had truncate the expansion at this first step, we would have got exactly our first approximation \eqref{eq:first_approximation}:
  \begin{equation*}
    \frac{\norm{f \sqrt w \nu * f \sqrt w \nu * f \sqrt w \nu}^2_{L^2(\R^2)} }{\norm{f}^6_{L^2(\R)}} \geq
    \frac{1}{c_0} \cdot c_0^2 = c_0
  \end{equation*}
  Now we can get a better approximation computing more coefficients.
\end{remark}

In order to compute $c_2$, we calculate the second moment $\mathcal{I}_2$ first:
\begin{equation*}
  \mathcal{I}_2 = \frac{3 \, G(0)^2 G^{''}(0)}{(3^{\frac34})^3 \, \Gamma(\frac74)} = \frac{3\, \Gamma(\frac{5}{12})^2 \Gamma(\frac{11}{12})}{2^3 3^{\frac94} \, \Gamma(\frac74)} = \alpha_2 \, c_0, \qquad \alpha_2 := \frac{2 \, \Gamma(\frac{11}{12}) \Gamma(\frac14)}{3\sqrt{3} \, \Gamma(\frac{5}{12}) \Gamma(\frac34)} .
\end{equation*}

The second coefficient squared is
\begin{equation*}
  \textstyle  c_2^2 = 5 \nang{g, \frac{1}{2} \, (3 t^2-1) }^2 =
  \frac{5}{4^{\color{blue}2}} ( 3 \mathcal{I}_2 - \mathcal{I}_0 )^2 
\end{equation*}

and the sum of the first two coefficients squares is
 \begin{equation*}
    c_0^2 + c_2^2 = \pa{ \frac94 - \frac{15}{4}\alpha_2 + \frac{45}{4^2} \alpha^2_2} c_0^2.
 \end{equation*}
This gives us the lower bound 
 \begin{equation*}
    \frac{\norm{f \sqrt w \nu * f \sqrt w \nu * f \sqrt w \nu}^2_{L^2(\R^2)} }{\norm{f}^6_{L^2(\R)}} \geq
    \pa{ \frac94 - \frac{15}{4}\alpha_2 + \frac{45}{4^2} \alpha_2^2 } c_0 \approx 0.2997842\dots
  \end{equation*}
  unfortunately still not large enough.

  % ------------------------------------- %

  \noindent We compute $c_4$. The fourth moment $\mathcal{I}_4$ is:
  \begin{equation*}
    \mathcal{I}_4 =
    \pa{ \frac{2^4}{15} \frac{\Gamma(\frac{11}{12})^2}{ \Gamma(\frac{5}{12})^2} + \frac{2}{3^3} } c_0 = \alpha_4 \, c_0. 
  \end{equation*}

  The fourth coefficient squared is
  \begin{equation*}
    \textstyle  c_4^2 = 9 \nang{g, \frac18 \, (35 t^4 - 30 t^2 +3)}^2 =
    \frac{9}{{\color{blue}4}} \frac{1}{8^2} ( 35 \mathcal{I}_4 - 30 \mathcal{I}_2 + 3 \mathcal{I}_0 )^2 .
  \end{equation*}

  % and the sum of the squares
  % \begin{equation*}
  %   c_0^2 + c_2^2 + c_4^2 = \pa{ \frac94 - \frac{15}{4}\alpha_2 + \frac{45}{4^2} \alpha^2_2} c_0^2.
  % \end{equation*}

  Then we obtain the lower bound we were looking for:
  \begin{equation*}
    \frac{\norm{f \sqrt w \nu * f \sqrt w \nu * f \sqrt w \nu}^2_{L^2(\R^2)} }{\norm{f}^6_{L^2(\R)}} \geq
    \pa{ \frac94 - \frac{15}{4}\alpha + \frac{45}{4^2} \alpha^2 + \frac{9}{2^8} ( 35 \alpha_4 - 30 \alpha_2 + 6 )^2 } c_0 \approx 0.306879 > \frac{\pi}{6 \sqrt{3}}.
  \end{equation*}

  

\newpage

\bibliographystyle{alpha}
\nocite{*}
\bibliography{bibliography}


\end{document}
