# use latexmk and be happy
# latexmk -pdf -pdflatex="pdflatex -interaction=batchmode" Thesis.tex
LATEX=pdflatex -interaction=batchmode
NAME=Thesis
FINALPDF=$(NAME).pdf
MAIN_TEX_SOURCE=$(NAME).tex
AUX_SOURCE=Chapters/*.aux *.aux
MAIN_AUX_SOURCE=$(NAME).aux
# INDEX_SOURCE=$(NAME).toc
SOURCE=*.tex Chapters/*.tex
BIBSOURCE=*.bib
TEMPFILES=*.out *.log *.toc *.nav *.snm  *~ Chapters/*.log $(NAME).blg
BIBLIOGRAPHY=$(NAME).bbl

# output color:
#COLOR="\033["
WHITE="\033[38;1m"
GREEN="\033[32;1m"
CLOSE="\033[0m"

# prefixes:
WORK=$(GREEN)"::"$(CLOSE)
INFO=$(GREEN)" >"$(CLOSE)


all: $(FINALPDF)
	@echo $(INFO) $(WHITE)"Compiled"$(CLOSE)

$(FINALPDF): $(BIBLIOGRAPHY) $(SOURCE) $(MAIN_AUX_SOURCE) # $(MAIN_TEX_SOURCE)
	@echo $(WORK) $(WHITE)"Compiling \c" $@ $(CLOSE); $(LATEX) $(MAIN_TEX_SOURCE) 2>&1 > /dev/null
	@echo "\t"$(GREEN)[$(CLOSE)$(WHITE)"Done"$(CLOSE)$(GREEN)]$(CLOSE)

$(MAIN_AUX_SOURCE): $(SOURCE)	# create AUX files
	@echo $(WORK) $(WHITE)"Compiling aux files \c" $@ $(CLOSE); $(LATEX) $(MAIN_TEX_SOURCE) 2>&1 > /dev/null
	@echo "\t"$(GREEN)[$(CLOSE)$(WHITE)"Done"$(CLOSE)$(GREEN)]$(CLOSE)

$(BIBLIOGRAPHY): $(MAIN_AUX_SOURCE) $(BIBSOURCE)
	@echo $(WORK) $(WHITE)"BibTeXing \c" $@ $(CLOSE); bibtex $(MAIN_AUX_SOURCE) 2>&1 > /dev/null
	@echo "\t"$(GREEN)[$(CLOSE)$(WHITE)"Done"$(CLOSE)$(GREEN)]$(CLOSE)
	@echo $(WORK) $(WHITE)"Recompiling \c" $@ $(CLOSE); $(LATEX) $(MAIN_TEX_SOURCE) 2>&1 > /dev/null
	@echo "\t"$(GREEN)[$(CLOSE)$(WHITE)"Done"$(CLOSE)$(GREEN)]$(CLOSE)

clean:
	@echo $(WORK) $(WHITE)"Clean useless files"$(CLOSE)
	@rm -f $(TEMPFILES) # $(FINALPDF)

erase:
	@echo $(WORK) $(WHITE)"Erase bib, temp & aux files"$(CLOSE)
	@rm -f $(FINALPDF) $(TEMPFILES) $(AUX_SOURCE) $(BIBLIOGRAPHY)

open: $(FINALPDF)
	@echo $(INFO) $(WHITE)"Opening $(FINALPDF)"$(CLOSE)
	@xdg-open $(FINALPDF)
