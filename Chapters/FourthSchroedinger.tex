\chapter{A family of fourth order Schrödinger equation}

We study a family of fourth order Schrödinger equations
in one dimension depending on the parameter $\mu \geq 0$\footnote{The case $\mu < 0$ is not considered since Strichartz estimates may not be available
  in view of the presence of the %degenerate
  critical point for the phase function, see \cite[Condition (2.1.c)]{kenig1991oscillatory}.}:
\begin{equation}
  \label{eq:4order_family}
  \begin{dcases}
    i \partial_t u -\mu \triangle u + \triangle^2 u = 0 \quad x,t \in \R \\
    u(0,x) = f(x) \in L^2(\R) 
  \end{dcases}
\end{equation}

% % Background and historical info here ...
% \vspace{2mm}
% This is the linear version of the equation introduced by Karpman and Shagalov for studying
% the propagation of intense laser beam.

% to take into account the role of “fourth-order dispersion” in the propagation of intense laser beams in
% a bulk medium with Kerr nonlinearity.
% \vspace{2mm}

The solution of the equation \eqref{eq:4order_family} is given by
\begin{equation}
  \label{eq:solution_family}
  S_\mu(t) f := e^{i t (\triangle^2 - \mu \triangle)}f = (e^{i(x\xi+t \phi_\mu(\xi))}\hat{f}(\xi))^{\widecheck{}} =  \int_\R e^{i(x\xi+t \phi_\mu(\xi))}\hat{f}(\xi) \td \xi \, , % normalization 2\pi ?
\end{equation}
where $\phi_\mu(\xi) = \xi^4 + \mu\xi^2$.
A family of Strichartz estimates is available:
\begin{equation}
  \label{eq:KPV_Strichartz_family}
  \norm{D_\mu^{\frac{\theta}{2}} S_\mu(t) f }_{L^q_t(\R)L_{x}^p(\R)} \leq c \Norm{f}_{L^2(\R)},
\end{equation}
where $(q,p) = \pa{\frac{4}{\theta},\frac{2}{1-\theta}}$, $\theta \in [0,1]$, and the operator $D_\mu^\alpha$ is given by
\begin{equation}\label{eq:fractional differential}
  D_\mu^\alpha f (x) := \int_\R e^{ix\xi} \abs{\phi_\mu''(\xi)}^{\frac{\alpha}{2}} \hat{f}(\xi) \d \xi .
\end{equation}
These estimates have been proven by Kenig, Ponce and Vega \cite[Theorem 2.1]{kenig1991oscillatory} for a broad class of phase function $\phi$.
In their paper the authors deal with global and local smoothing properties
of dispersive equations.
These results are obtained exploiting the decay of the oscillatory integral representing the solution. 

\noindent 
\begin{minipage}{0.55\linewidth}
    In our case, when the parameter $\theta$ ranges in $[0,1]$, we obtain all the points in line
    connecting $(\frac12,0)$ and $(0,\frac14)$ in the diagram.
    We are mainly interested in the symmetric case, when $q~=~p~=~6$,
    and the inequality \eqref{eq:KPV_Strichartz_family} is:
  \begin{equation}
    \label{eq:Strichartz_family}
    \norm{D_\mu^{\frac13} S_\mu(t) f }_{L_{t,x}^6(\R \times \R)} \leq C \Norm{f}_{L^2(\R)}.
  \end{equation}
    This case (in {\color{blue} blue} in the diagram) is obtained when $\theta = \frac23$.
\end{minipage}  \hspace{5mm} % niente a capo qui
\begin{minipage}{0.4\linewidth}
  \vspace{6mm}
    \begin{tikzpicture}[domain=0:5,scale=1.8] 
      \draw[->] (0,0) -- (2.5,0) node[right] {$\frac 1 p$}; 
      \draw[->] (0,0) -- (0,1.5) node[left] {$\frac 1 q$};

      % x axes
      \node (punto) at (2,0)[label=below:$\frac 1 2$] {$\bullet$};

      % y axes
      \node (punto) at (0,1)[label=left:$\frac 1 4$] {$\bullet$};
      
      %% punti
      \node (punto) at (0.66,0.66)[label=above:${\color{blue}\pa{\frac 16,\frac16}}$]{${\color{blue}\bullet}$};
      
      % linee
      \draw (2,0) -- (0,1);
      \draw[dashed, color=blue] (0,0) -- (1.2,1.2);
    \end{tikzpicture}
    \captionof{figure}{Riesz diagram for the Strichartz estimates in \eqref{eq:KPV_Strichartz_family}.}
  \end{minipage}\\

  Recently, in \cite{quilodran2016extremizers} the authors studied the problem of existence of extremizers for the Strichartz estimates for the same family \eqref{eq:4order_family} but in dimension $2$.
  By using a dichotomy result about existence of extremizers \cite[Section 4]{jiang2014linear}
  for the corresponding Strichartz inequality
  they proved that maximizers exist when $\mu = 0$, and fail to exist when $\mu = 1$ (and, by scaling, when $\mu >0$).

  Aiming to a similar result for the $1$-dimensional problem,
  in this thesis we study the problem of maximizers for the endpoint $\mu=0$.

\section{Pure power of Laplacian}
From now on we will consider the case $\mu = 0$ for which our family \eqref{eq:4order_family} reduces to the equation:
\begin{equation}\label{eq:mu=0}
  \begin{dcases}
    i \partial_t u + \triangle^2 u = 0 \qquad x,t \in \R \\
    u(0,x) = f(x) \in L^2(\R) .
  \end{dcases}
\end{equation}
The solution is given by
\begin{equation}
  \label{eq:solution_mu=0}
  S_0(t) f := e^{i t \triangle^2}f = (e^{i(x\xi + t \xi^4)}\hat{f}(\xi))^{\widecheck{}} = \int_\R e^{i(x\xi + t \xi^4)}\hat{f}(\xi) \td \xi \, .
\end{equation}
As before, for the solution we have the corresponding Strichartz estimate:
\begin{equation}
  \label{eq:Strichartz}
  \norm{D_0^{\frac13} e^{i t \triangle^2} f }_{L_{t,x}^6(\R \times \R)} \leq \SharpC \Norm{f}_{L^2(\R)} ,
\end{equation}
where the operator $D_0^{\frac13}$ is defined as
\begin{equation*}
  D_0^{\frac13} f (x) := \int_\R e^{ix\xi}  \abs{6\xi^2}^{\frac{1}{6}} \hat{f}(\xi) \d \xi .
\end{equation*}
\begin{remark}
  Here the operator $D_0^{\frac13} = 6^{\frac16} \abs{\nabla}^{\frac13}$.
  This operator differs from the one in \eqref{eq:fractional differential} and in \cite{kenig1991oscillatory} by a factor of $2^{\frac16}$.
\end{remark}
For the convenience of the reader, we indicate with $T(t)$ the propagator given by the composition $D_0^{\frac13} S_0(t) = D_0^{\frac13} e^{it\triangle^2}$.
  This is defined as
  \begin{equation}\label{def:T}
    T(t) f (x) := \int_\R e^{ix\xi} 6^{\frac16} \sqrt{w(\xi)} e^{it\xi^4} \hat{f}(\xi) \td \xi, \qquad w(\xi) = \abs{\xi}^{\frac23}.
  \end{equation}
  Sometimes we will omit the time variable $t$ writing $Tf$ in place of $T(t) f$.
  \vspace{2mm}

% We are interest in finding the sharp constant $\SharpC$ in \eqref{eq:Strichartz} and studying the extremizers.
The optimal constant $\SharpC$ in \eqref{eq:Strichartz} is defined as
\begin{equation}
  \label{eq:sharp_constant}
  \SharpC := \sup_{\substack{f \in L^2(\R), \\ f \neq 0}} \frac{ 6^{\frac16} \, \norm{\abs{\nabla}^{\frac13} e^{i t \triangle^2} f}_{L_{t,x}^6(\R \times \R)}}{\Norm{f}_{L^2(\R)}} .
\end{equation}

We write this quantity in a different way.
First, we expand the solution $T(t)f$ in \eqref{def:T} using the extension operator $\mathcal R^\star$ defined in \eqref{eq:extension_operator}.
Let $\nu(\xi,\tau) = \ddelta{\tau - \xi^4}$. Then
\begin{equation*}
  T(t) f = D_0^{\frac13} e^{it\triangle^2} f = \Fourier^{-1}_{t,x}\pa{2\pi\, \hat{f}(\xi) \sqrt{w(\xi)} \nu(\xi,\tau)} .
\end{equation*}
Since $\norm{\,\cdot\,}_{L^6}^3 = \norm{ [\,\cdot\,]^3 }_{L^2}$, we get
\begin{equation*}
  \SharpC^3 = \sup_{f \in L^2} \frac{ 6^{\frac12} \, \norm{\,\abs{\nabla}^{\frac13} e^{i t \triangle^2} f}_{L_{t,x}^6(\R^2)}^3}{\Norm{f}_{L^2(\R)}^3} =
   \sup_{f \in L^2} \frac{ 6^{\frac12} \, \norm{ \bra{\Fourier^{-1}(2\pi\, \hat{f}(\xi) \sqrt{w(\xi)} \nu(\xi,\tau))}^3 }_{L_{t,x}^2(\R^2)}}{\Norm{f}_{L^2(\R)}^3} .
\end{equation*}
We focus on the quantity inside the norm in the numerator.
By applying Plancherel \eqref{eq:Plancherel} in dimension $2$ we have
% \begin{equation*}
%   \norm{\,\cdot\,}_{L^2(\R^2)} = (2\pi)^{-1} \norm{ \Fourier[\,\cdot\,] }_{L^2(\R^2)} ,
% \end{equation*}
% Applying the (space-time) Fourier transform we obtain
\begin{equation*}
 \norm{ \bra{\Fourier^{-1}(2\pi\, \hat{f}(\xi) \sqrt{w(\xi)} \nu(\xi,\tau))}^3 }_{L_{t,x}^2(\R^2)} =
(2\pi)^{-1} \norm{ \Fourier\bra{\Fourier^{-1}(2\pi\, \hat{f}(\xi) \sqrt{w(\xi)} \nu(\xi,\tau))}^3 }_{L_{t,x}^2(\R^2)}
 \end{equation*}

\noindent We use the convolution identity for the Fourier transform \eqref{eq:Convolution} twice and the Inversion formula \eqref{eq:Inversion} to obtain
 \begin{align*}
   (2\pi)^{-1} \norm{\Fourier\bra{\Fourier^{-1}(2\pi\, \hat{f} \sqrt{w} \nu}^3 }_{L_{t,x}^2(\R^2)} & = (2\pi)^{-1} \norm{(2\pi)^{-1} \Fourier\bra{\Fourier^{-1}(2\pi\, \hat{f} \sqrt{w} \nu )}^2 * \hat{f} \sqrt{w} \nu}_{L_{t,x}^2}  \\
   % & = \sup \frac{6^{\frac12}}{2\pi} \frac{\norm{(2\pi)^{-1} \hat{f} \sqrt{w} \nu * ((2\pi)^{-2} 2\pi\, \hat{f} \sqrt{w} \nu  * 2\pi\, \hat{f} \sqrt{w} \nu )}_{L_{t,x}^2}}{\Norm{f}_{L^2}^3} \\
   & = (2\pi)^{-1} \norm{(2\pi)^{-1} \hat{f} \sqrt{w} \nu * (\hat{f} \sqrt{w} \nu  * \hat{f} \sqrt{w} \nu ) }_{L_{t,x}^2} \\
   & = (2\pi)^{-2} \norm{ \hat{f} \sqrt{w} \nu * (\hat{f} \sqrt{w} \nu  * \hat{f} \sqrt{w} \nu )}_{L_{t,x}^2}.
\end{align*}

\noindent We apply the Fourier transform  to the denominator, and again by Plancherel we have
\begin{equation*}
  \Norm{f}_{L^2(\R)}^3 =  (2\pi)^{-\frac32}\norm{\hat{f}\,}_{L^2(\R)}^3 .
\end{equation*}
Finally, since the Fourier transform is a bijection on $L^2$:
\begin{equation*}
  \SharpC^3 = \sqrt{\frac{6}{2\pi}} \sup_{f \in L^2} \frac{\norm{ \hat{f} \sqrt{w} \nu * \hat{f} \sqrt{w} \nu  * \hat{f} \sqrt{w} \nu }_{L_{t,x}^2(\R^2)}}{\norm{\hat{f}\,}_{L^2(\R)}^3} = \sqrt{\frac{3}{\pi}} \sup_{f \in L^2} \frac{\norm{ f \sqrt{w} \nu * f \sqrt{w} \nu  * f \sqrt{w} \nu }_{L_{t,x}^2(\R^2)}}{\Norm{f}_{L^2(\R)}^3} .
\end{equation*}
Taking the square to both sides, we have:
\begin{equation}\label{eq:sharpCequiv}
  \frac{\pi}{3} \, \SharpC^6 = \sup_{\substack{f \in L^2(\R), \\ f \neq 0}} \frac{\norm{ f \sqrt{w} \nu * f \sqrt{w} \nu  * f \sqrt{w} \nu }_{L_{t,x}^2(\R^2)}^2}{\Norm{f}_{L^2(\R)}^6}
\end{equation}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../Thesis"
%%% End:
