\section{Strichartz estimates}
\noindent The Strichartz estimates are a family of inequalities for dispersive equations in which the
norm of the solution is taken in the mixed Lebesgue space $L^q_t(\R)L^p_x(\R^d)$.

They were introduced by Robert Strichartz in his seminal paper \cite{strichartz1977restrictions}
in which he pointed out the connection between restriction theory of the Fourier transform
and the decay of the solution of wave and Schrödinger equations.

\subsection{Strichartz estimates for Schrödinger equation}
The estimate we obtained in \eqref{TS to Schroedinger} using restriction theory
is quite remarkable, nevertheless \cref{Th:dualTS} gives estimates % in time and space
only on isotropic Lebesgue space (on $L^q_t(\R)L^p_x(\R^d)$ when $q = p$).
On the other hand, the paraboloid is invariant under the anisotropic scaling 
\[ (x,t) \rightsquigarrow (\lambda x , \lambda^2 t) .\]
The solution of the Schrödinger equation \eqref{eq:sol2SE}, as a Fourier extension from a paraboloid,
enjoys this not linear dilation in the time coordinate.
So it is reasonable to study restriction and extension estimates on anisotropic spaces, i.e. when $q \ne p$.
In fact, the solution of the Schrödinger equation \eqref{eq:sol2SE} enjoys
the following estimates:
\begin{equation} \label{eq:strichartz} % \tag{Strichartz}
 \scalebox{1.2}{$\lVert e^{-it\Delta} u_0 \rVert_{L^q_t(\R)L^p_x(\R^d)} \leq C \norm{u_0}_{L^2(\R^d)}$} .
\end{equation}

  \vspace{3mm}\noindent
  \begin{minipage}{0.65\linewidth}
    The constant $C = C(q,p,d)$ depends on exponents and dimension.
    By scaling \eqref{eq:strichartz}, we obtain the condition:  
    \begin{equation}
      \frac 2 q + \frac d p = \frac d 2 \quad \text { with } \quad
      \begin{cases}
        p \in [2,\infty] & \text{ if } \; d = 1\\
        p \in [2,\infty) & \text{ if } \; d = 2\\
        p \in \left[2,\frac{2d}{d-2} \right] & \text{ if } \; d \ge 3
      \end{cases}\label{eq:strichartz_condition}
    \end{equation}
For a given dimension $d$, a pair $(q,p)$ satisfying the above relation is called \emph{admissible}.
\end{minipage}\hspace{-8mm}
\begin{minipage}{.5\linewidth}
  \begin{center}
    \begin{tikzpicture}[domain=0:5,scale=1.5] 
      \draw[->] (0,0) -- (2.5,0) node[right] {$\frac 1 p$}; 
      \draw[->] (0,0) -- (0,2.5) node[left] {$\frac 1 q$};
      % x axes
      \node (punto) at (2,0)[label=below:$\frac 1 2$] {$\bullet$};
      \node (punto) at (0.8,0)[label=below:$\frac{d-2}{2d}$]{$\vert$};
      % y axes
      \node (punto) at (0,1)[label=left:$\frac 1 4$] {$\bullet$};
      \node (punto) at (0,2)[label=left:$\frac 1 2$] {$\circ$};
      \draw[dashed] (0,2) -- (2,2){};
      %% punti
      \node (punto) at (0.8,2)[label=above:$\pa{\frac 1 2,\frac{d-2}{2d}} $]{${\color{red}\bullet}$}; % an endpoint      
      % linee and nodes
      \draw (2,0) -- node[scale=.7, above, rotate=-25] {$d=1$} (0,1){};
      \draw (2,0) -- node[scale=.7, above, rotate=-45] {$d=2$} (0,2){};
      \draw (2,0) -- node[scale=.7, above, rotate=-60] {$d \geq 3$} (0.8,2){};
      % \node[above, rotate=-25] at (0.4,.8) {$d=1$};
      % \draw (2,0) -- (0,1){};
      % \draw (2,0) -- node[above, rotate=-45] {$d=2$} (0,2){};
      % \draw (2,0) -- (0.8,2){};
      % \node[above, rotate=-55] at (1.7,.5) {$d \geq 3$};
    \end{tikzpicture} 
  \end{center}
\end{minipage}\vspace{3mm}

\begin{remark}
  In $d=2$ the endpoint $(q,p) = (2,\infty)$ has been proved false
  by Montgomery-Smith \cite{montgomery1997time}.
  For $d \ge 3$, the endpoint $(q,p) = \pa{2, \frac{2d}{d-2}}$
  has been proved by Keel and Tao \cite{keel1998endpoint}.
\end{remark}

Proving this inequality is equivalent to showing either of the following:
\begin{align*}
&\bullet &\hspace{-3em}
 T&:=e^{-it\Delta} && \hspace{-4mm}\colon  L^2(\R^d)  \longrightarrow  L^q_tL^p_x(\R\times \R^{d}) &&\text{ is bounded, \phantom{operator}}\\
&\bullet &  \hspace{-1em}T^\star&:=(e^{-it\Delta})^\star &&\hspace{-3mm}\colon  L^{q'}_t L^{p'}_x(\R\times \R^{d}) \to L^2(\R^d) && \text{ is  bounded. \phantom{operator}}
\end{align*}
The composition $TT^\star$ :
\begin{align*}
  &\bullet & TT^\star& = e^{-it\Delta} (e^{-is\Delta})^\star&&
         \hspace{-1em}\colon L^{q'}_t L^{p'}_x(\R\times \R^{d}) \to L^q_tL^p_x(\R\times \R^{d}) \quad \text{ is a bounded operator}.
\end{align*}

\noindent We prove the last bound for $TT^\star$ and, by Hölder and duality, the previous follow.
\begin{teo}[Nonendpoint Strichartz estimates]\label{Th:TTstar}
  The operator $TT^\star$ is given by
  $u \mapsto \int_{-\infty}^{+\infty} e^{-i(t-s)\Delta} \, u \d s $
  and the following inequality holds
  \begin{equation}\label{TTstar}
    \Norm{ \int_{-\infty}^\infty e^{-i(t-s)\Delta} F(s) \d s}_{L^q_tL^p_x(\R \times \R^{d})} \ls \norm{F}_{L^{q'}_tL^{p'}_x(\R \times \R^{d})}
  \end{equation}
  for $(d,q,p)$ satisfying the condition \eqref{eq:strichartz_condition}.
\end{teo}
\begin{remark}
  The bound \eqref{TTstar} is closely related
  to the bound for the solution of the inhomogeneous Schrödinger equation:
  \[  \begin{cases}
      i \partial_t u - \triangle u = F \\
      u(0,x) = u_0(x) 
    \end{cases}
  \]
  which by Duhamel's formula is
  \begin{equation}\label{Duhamel}
    u(t,x) = e^{-it\Delta}u_0 + i \int_0^t e^{-i(t-s)\Delta} F(s) \d s .
  \end{equation}
\end{remark}
We start proving $L^p$-bounds for the kernel in \eqref{TTstar}.
Details of the proof can be found in \cite{linares2014introduction}. 
\begin{lemma}\label{Lemma:space_estimates}
  The operator $e^{it\Delta}$ defined on $\Schwartz(\R^d)$ % to $\Schwartz(\R^d)$ and
  extends to an unitary operator on $L^2(\R^d)$. Moreover, % for every $v \in L^2(\R^d)$,
  we have the following estimates:
  \begin{center}
    \begin{tabular}{c c c}
      $\displaystyle \norm{e^{-it\Delta}v}_{L^2} = \norm{v}_{L^2}$ & \hspace{1cm} & $\displaystyle \norm{e^{-it\Delta}v}_{L^\infty} \le (4 \pi \abs{t})^{- \frac d 2} \norm{v}_{L^1}$. \\
      Energy estimate & & Decay estimate
    \end{tabular}
  \end{center}
  Interpolating between them for $2 \le p \le \infty$ we obtain:
  \[ \norm{e^{-it\Delta}v}_{L^{p}} \le \pa{4 \pi \abs{t}}^{- d\pa{\frac 1 2 - \frac 1 p }} \norm{v}_{L^{p'}} .\]
\end{lemma}

    \begin{proof}[Proof of \cref{Th:TTstar}]
      % The equivalence of the \eqref{Th:1}, \eqref{Th:2} and \eqref{Th:3} follows from duality and Hölder.\\
      We prove the theorem in all case but the endpoint.\\
      From \cref{Lemma:space_estimates} applied to \eqref{TTstar} we have:
      \[ \Norm{ \int_{-\infty}^\infty e^{-i(t-s)\Delta} F(s) \d s }_{L^{p}_x} \le \int_{-\infty}^\infty \pa{4 \pi \abs{t-s}}^{-d \pa{\frac{1}{2} - \frac 1 p}}\norm{F(s)}_{L^{p'}_x} \d s .\]
      The right hand side can be expressed as a convolution.
      Let $A$ be the left hand side and let
      $f(t) = \norm{F(t)}_{L^{p'}_x}$ and
      $g(t) = \pa{4 \pi \abs{t}}^{-d \pa{\frac 1 2 - \frac 1 p }}$, then   
      \[ \norm{ A }_{L^q_t(\R)} \ls \norm{f * g}_{L^q_t(\R)} .\]

      Using \emph{weak Young inequality}\footnote{or by applying Hardy-Littlewood-Sobolev lemma} for $r > 1$:
      \[ \norm{f * g}_{L^q} \le \norm{f}_s \norm{g}_{r,\infty} \quad \text{ for all } \; (s,r) \, : \, \frac 1 s + \frac 1 r = 1 + \frac 1 q .\]
      %Since $\abs{t}^{-1} \in L^{1,\infty}(\R)$, $\abs{t}^{- 1/r}  \in L^{r,\infty}(\R)$.
      In our case $g \in L^{r,\infty}(\R)$ where $\frac1r=d\pa{\frac 12 -\frac1p}$.
      Notice that, by scaling, we have      
      \begin{equation*}
        \frac 1 q = \frac d2 \pa{ \frac 1 2 - \frac 1 p } \quad \text{ then } \quad \frac 2 q = \frac 1 r ,
      \end{equation*}
      which implies $s = q'$. Thus
      \[ \norm{ A }_{L^q_t(\R)} \ls \norm{f * g}_{L^q_t(\R)} \ls \norm{f}_{q'} \norm{g}_{r,\infty} =  \norm{F}_{L^{q'}_tL^{p'}_x(\R \times \R^d)} .\]
      This proves the estimate away from the endpoint.
    \end{proof}
    

\section{Extremizers for 
Strichartz estimates } %for Schrödinger equation}
We are interested in the best value of the constant in the inequality \eqref{eq:strichartz}.
This will be defined as
\begin{equation}\label{eq:bestconst}
\mathbf{C} := \sup_{u_0 \in L^2(\R^d) \setminus \{0\}} \frac{\lVert e^{-it\Delta} u_0 \rVert_{L_t^q(\R)L_x^p(\R^d)}}{\norm{u_0}_{L^2(\R^d)}} .
\end{equation}

\begin{define}
  A nonzero function $f$ that realises equality in an inequality
  is called \emph{extremizer} or \emph{maximizer} for that inequality.
\end{define}
In particular, we look for a nonzero $f \in L^2(\R^d)$ that realises equality in \eqref{eq:strichartz},
if there exist one.
Even if maximizers do not exist, we always have sequences of functions maximising  \eqref{eq:bestconst}.
\begin{define}
  A sequence $\{f_n\}_{n \in \N}$, with $\norm{f_n}_{L^2} \leq 1$ is an \emph{extremizing sequence} for \eqref{eq:strichartz} if
  \begin{equation*}
    \lim_{n \to \infty} \norm{ e^{-it\Delta} f_n}_{L_t^q(\R)L_x^p(\R^d)} \to \mathbf{C} .
  \end{equation*}
\end{define}

\begin{remark}
  Due to the several symmetries of the solution, extremizing sequences may not converge to an extremizer in the strong topology. % So we can calculate the sharp constant, but finding extremizers (when they exist) is a more subtle task. 
\end{remark}

Despite of the difficulties, existence of extremizers for the Strichartz estimate \eqref{eq:strichartz} has been proved
in \emph{all} dimensions!
The first result, in dimension $1$, was given by Kunze \cite{kunze2003existence}: he proved existence of extremizers exploiting the concentration-compactness principle of Lions \cite{Lions1985}. Then Foschi  \cite{foschi2004maximizers} managed to characterise maximizers in dimensions $d=1$ and $2$, showing that they are\footnote{up to the symmetries of the solution \cref{list of symmetries}.} Gaussians.
A few years later Shao \cite{shao2008maximizers} proved the existence of extremizers in every dimension, and not only in the symmetric case but for \emph{any} non-endpoint admissible pair $(q,p)$.

\noindent 
\begin{minipage}{0.55\linewidth}    
    The figure shows admissible pairs of exponent $(q,p)$ for different dimensions.\\
    The blue {\color{blue} dots} on the diagonal represent the symmetric exponents $(q,q)$,
    for which we can use restriction theory.\\

    Characterise the extremizers in higher dimension is still an open problem.
    % The relation between $q,p$ and $d$ can be obtained by scaling \eqref{eq:strichartz}.
\end{minipage} % niente a capo qui
\begin{minipage}{0.5\linewidth}
  \vspace{6mm} \hspace{10mm}
    \begin{tikzpicture}[domain=0:5,scale=1.8] 
      \draw[->] (0,0) -- (2.5,0) node[right] {$\frac 1 p$}; 
      \draw[->] (0,0) -- (0,2.5) node[left] {$\frac 1 q$};

      % x axes
      \node (punto) at (2,0)[label=below:$\frac 1 2$] {$\bullet$};
      % \node (punto) at (0.8,0)[label=below:$\frac{d-2}{2d}$]{$\vert$};

      % y axes
      \node (punto) at (0,1)[label=left:$\frac 1 4$] {$\bullet$};
      \node (punto) at (0,2)[label=left:$\frac 1 2$] {$\circ$};
      % \draw[dashed] (0,2) -- (2,2){};
      
      %% punti
      \node (punto) at (0.8,2)[label=above:$\pa{\frac 1 2,\frac{d-2}{2d}} $]{${\bullet}$}; % an endpoint
      \node (punto) at (0.66,0.66){${\color{blue}\bullet}$};
      \node (punto) at (1,1){${\color{blue}\bullet}$};
      \node (punto) at (1.25,1.25){${\color{blue}\bullet}$};
      
      % linee
      \draw (2,0) -- (0,1)[label=$d=1$]{};
      \draw (2,0) -- (0,2)[label=left:$d=2$]{};
      \draw (2,0) -- (0.8,2){};

      \draw[dashed, color=blue] (0,0) -- (2,2){};
    \end{tikzpicture}
    % \captionof{figure}{Admissible pairs $(p,q)$.}
  \end{minipage}\\

  \iffalse % ----------------- %
  
  Lets focus on the symmetric case, when $p = q$ and the inequality \eqref{eq:strichartz} becomes:
\begin{equation} \label{eq:sym_strichartz} 
 \lVert e^{-it\Delta} u_0 \rVert_{L^{q(d)}(\R^{d+1})} \leq C \norm{u_0}_{L^2(\R^d)} , \qquad q(d) = 2 + \frac 4 d .
\end{equation}
  
\noindent It is possible to write the solution using the adjoint Fourier restriction operator: % from the paraboloid $\Sigma = \{(\tau,\xi) \in \R^{d+1}, \tau = \abs{\xi}^2 \}$. We have: % with the measure $\sigma(\tau,\xi) = 2\pi \, \delta(\tau - \abs{\xi}^2)$:
\[ u(t,x) = \int_{\R^d} e^{i(x \cdot \xi + t\abs{\xi}^2)}\widehat{u_0}(\xi) \text{ \dj} \xi =  \int_{\R^{d+1}} e^{i(t,x)\cdot(\tau,\xi)}  \, 2\pi \, \widehat{u_0}(\xi) \, \delta(\tau - \abs{\xi}^2) \text{ \dj} \tau \text{ \dj} \xi ,\]
where $\text{\dj} \xi$ and $\text{\dj} \tau$ indicate the measures $\frac{\d\xi}{(2 \pi)^d}$ and $\frac{\d\tau}{2 \pi}$. Then the solution is an extension of a measure from the paraboloid $\Sigma = \{(\tau,\xi) \in \R^{d+1}, \tau = \abs{\xi}^2 \}$ and we can write as
\begin{equation}
 u = \Fourier^{-1}(\underbrace{2 \pi \, \widehat{u_0}}_{f(\xi)} \, \underbrace{\delta(\tau - \abs{\xi}^2)}_{\sigma_0(\xi,\tau)}) = \Fourier^{-1}(f \, \sigma_0).\label{eq:sol_as_restriction}
\end{equation}
In dimension $d=1,2$ the Strichartz exponent $q(d)$ is a even number, so % $6$ and $4$.
we can exploit Plancherel in $L^2$. In dimension $d=2$ for example:
\begin{equation}
  \label{eq:L4->L2}
  \norm{ u }_{L^4(\R^3)} = %\sqrt{\norm{ \, u^2 }}_{L^2(\R^3)} =
  \norm{ u \cdot  u }^{\frac 12}_{L^2(\R^3)} = \norm{ \Fourier(u) * \Fourier(u)}^{\frac 1 2}_{L^2(\R^3, \text{ \dj} \xi)} \overset{\eqref{eq:sol_as_restriction}}{=} \norm{ f \sigma_0 * f \sigma_0 }^{\frac 12}_{L^2(\R^3, \text{ \dj} \xi)} .
\end{equation}
Then the problem of estimating the $L^4$-norm of the solution $u$ reduces to estimate the $L^2$-norm of the convolution of a measure with itself.
From now on we will focus only on this $2$-dimensional case.
% \noindent The measure $\sigma * \sigma$ is supported on $\{ (\tau, \xi) \in \R \times \R^2 \, : \, 2 \tau \ge \abs{\xi}^2\}$ and we can write the quantity above as
% \[  \norm{ f \sigma * f \sigma }^2_{L^2(\R^3)} = \frac{1}{2 \pi} \iint_{\R^2 \times \R^2} \hat{f}(\eta) \hat{f}(\zeta) \delta\pa{\dots} = \ang{f \otimes f, 1 \otimes 1 }_{(\tau, \xi)} \, , \text{ where } \mu_{\tau,\xi} = \delta\pa{} \d \eta \d \d \zeta .\]


\subsection{The approach by Foschi}
In his paper \cite{foschi2004maximizers} in 2004 Damiano Foschi characterised extremizers of the Strichartz estimates in dimension $1$ and $2$.   
He used $\delta$-calculus in order to rewrite the convolution from above as
\begin{equation*}
  (f \sigma * f \sigma)(x,t) = \iint_{\R^2 \times \R^2} f(z)f(y) \, \delta(t-\abs{y}^2-\abs{z}^2)\delta(x-y - z) \d z \d y .
\end{equation*}
Writing the expression as a $L^2$-product with respect the measure $\mu(x,t) = \delta(t-\abs{y}^2-\abs{z}^2)\delta(x-y - z) \d z \d y $, %it is equal to $\ang{f \otimes f , 1 \otimes 1}_{L^2(\mu(x,t))}$.
and applying Cauchy-Schwarz we obtain:
\begin{equation*}
  \label{eq:CS}
  \ang{f \otimes f , 1 \otimes 1}_{L^2(\mu(x,t))} \leq \norm{f \otimes f}_{L^2(\mu(x,t))} \norm{1 \otimes 1}_{L^2(\mu(x,t))},
\end{equation*}
that is
\begin{equation*}
  \abs{f \sigma_0 * f \sigma_0 }^2(x,t) \leq (\abs{f}^2\sigma_0 * \abs{f}^2\sigma_0)(x,t)  \, (\sigma_0 * \sigma_0)(x,t) .
\end{equation*}
To study the convolution of the singular measure $\sigma_0(t,x) = \delta(t - \abs{x}^2)$, we will use some special symmetries of the problem:
\begin{align}
  (\sigma_0 * \sigma_0)(t,x) = \iint_{\R^2 \times \R} \delta(t - \tau - \abs{x - y}^2) & \, \delta(\tau - \abs{y}^2) \d \tau \d y && = \int_{\R^2} \delta(t - \abs{y}^2 - \abs{x - y}^2) \d y \nonumber \\
  \text{Using \emph{Galilean invariance}:} \quad (t,x) &\mapsto (t - \abs{x}^2, 0) && =  \int_{\R^2} \delta(t - \abs{x}^2 - 2\abs{y}^2) \d y \label{eq:pre_polar} \\
  \text{and \emph{parabolic dilation}:} \quad (t,x) &\mapsto (\lambda^2 t, \lambda x) && =  \int_{\R^2} \delta(1 - 2\abs{y}^2) \d y \nonumber \\
  \text{changing in polar coordinates: } \quad y &\mapsto r \omega , \, \omega \in \Sphere^1 && = \int_{\Sphere^1} \int_0^\infty \delta(1 - 2 r^2) \, r \d r \d \omega = \frac \pi 2 . \nonumber
\end{align}

\noindent The convolution $\sigma_0 * \sigma_0$ turn out to be \emph{constant} in every point $(t,x)$ of its support, so
\[ \norm{f \sigma_0 * f \sigma_0 }^2_{L^2(\R^3)} \leq \pa{\sup_{(\xi , \tau) \in \R^3} \abs{\sigma_0 * \sigma_0}(\xi,\tau) }\, \norm{f}^4_{L^2(\R^2)} = \frac \pi 2 \, \norm{f}^4_{L^2(\R^2)} .\]
The constant above was proven sharp and the extremizers are the functions that realise equality in the Cauchy-Schwarz. Solving a functional equation they are showed to be Gaussians.  


\section{Perturbed Schrödinger equation} % paraboloid \, $\Sigma_\phi = \{(t,x) : t = \abs{x}^2 + \phi(x) \}$}
Consider now the perturbed Schrödinger equation in $\R^d$:
\begin{equation}
  \begin{cases} \label{PSE} % \tag{Perturbed Schrödinger}
    i \partial_t u - \triangle u + M_\phi u = 0 \, , \hspace{2cm}  \widehat{M_\phi u}(x) = \phi(\xi) \widehat{u}(\xi) \\
    u(0,x) = u_0(x) \,,\quad u_0 \in \Schwartz(\R^d).
  \end{cases}
\end{equation}
The solution is given by $ u(t,x) = (e^{it(\abs{\xi}^2 + \phi(\xi) )}\widehat{u_0}(\xi))^{\widecheck{}} $ and equals the Fourier extension of $\, \widehat{u_0} \, $ %$\mathcal R^\star(\hat u_0)$
from the perturbed paraboloid
\[ P_\phi = \{(\xi,\tau) \in \R^{d+1}, \tau = \abs{\xi}^2 + \phi(\xi) \} .\]

Strichartz estimates are available also in this case. % citation
Then we want to calculate the sharp constant and characterise the extremizers for the corresponding Strichartz estimate:
\begin{equation}
  \label{eq:perturbedStrichartz}
  \lVert e^{-it\Delta} f \rVert_{L^{q(d)}(\R^{d+1})} \leq \mathcal C \norm{f}_{L^2} .
\end{equation}

As before, we study the convolution of the measure $\sigma(t,x) = \delta(t - \abs{x}^2 - \phi(x))$ with itself, but when $\phi \neq 0$ the paraboloid $P_\phi$ does not enjoy anymore the nice symmetries that we exploited before.
In order to obtain an expression similar to \eqref{eq:pre_polar} we operate a change of variables who plays the role of a ``Galilean boost'' for the perturbed case.
This transformation allows to compare pointwise the convolution $\sigma * \sigma$ with the unperturbed one:
\begin{teo}[Comparison principle]
  Let be $\phi \in C^1(\R^2)$ strictly convex, nonnegative function. Then
  \begin{equation} \label{eq:comprinc}
    (\sigma * \sigma)(\xi, \tau) \leq (\sigma_0 * \sigma_0)(\xi, \tau - 2 \phi(\xi /2)).
  \end{equation}
\end{teo}
\begin{proof}
Indicate with $\psi(y)$ the function $\abs{y}^2 + \phi(y)$. 
\begin{align*}
  (\sigma * \sigma)(\xi, \tau) & = \int_{\R^2} \delta(\tau - \psi(y) - \psi(\xi-y)) \d y \\
  & = \int_{\R^2} \delta(\tau - \psi(\xi/2 + y) - \psi(\xi/2 - y)) \d y .
\end{align*}
Where we operated the change $y = \lambda(w)w$, in which $\lambda$ is such that
\[ \psi(\xi/2 + \lambda w) + \psi(\xi/2 - \lambda w) - 2 \psi(\xi/2)= 2 \abs{w}^2.\]
After applying the transformation we are able to use polar coordinates:
\begin{align*}
  (\sigma * \sigma)(\tau,\xi) & = \int_{\R^2} \delta(\tau - 2\psi(\xi/2) - 2\abs{w}^2) \, \abs{ \Jac(\lambda w) } \d w \\
  & = \int_{\Sphere^1} \int_0^\infty \delta(\tau - 2\psi(\xi/2) - 2 r^2) \, \abs{ \Jac(\omega,r) } \, r \d r \d \omega \\
  & = \int_0^\infty \delta(\tau - 2\psi(\xi/2) - 2 r^2) \, \pa{ \int_{\Sphere^1} \abs{ \Jac(\omega,r) } \d \omega } \, r \d r
\end{align*}

\iffalse
Because of the midpoint convexity of $\psi$, the function
\begin{equation*}
  \Psi(\lambda w) := \psi(\xi/2 - \lambda w) + \psi(\xi/2 + \lambda w) - 2 \psi(\xi /2)
\end{equation*}

is such that
\[ \Psi(0) = 0 \qquad \text{ and } \qquad \lim_{\lambda \to +\infty} \Psi(\lambda w) = + \infty , \]
so for any $a > 0$ there exists $\lambda_a >0 $ such that $\Psi(\lambda_a w) = a$.

In particular, there exists a $\lambda = \lambda(w)$ such that $\Psi(\lambda w) = 2 \abs{w}^2$.

The transformation we are looking for is given by
\[ y = T(w) \quad \text{ where } \,  T(w) := \lambda w .\] 
\fi

\noindent Computing explicitly the Jacobian for every fixed $\xi \in \R^d$, it turns out that  $\abs{\Jac(y)} < 1$ for every $y \neq 0$.
Then the convolution of perturbed measure is pointwise bounded by
\[ (\sigma * \sigma)(\xi, \tau) \leq (\sigma_0 * \sigma_0)(\xi, \tau - 2 \phi(\xi/2)) \]
\end{proof}

\begin{remark}
  This result holds in every dimension $d \geq 2$, when we perturbed the paraboloid with a strictly convex, nonnegative function $\phi \in C^1(\R^d)$.
\end{remark}

\begin{remark}
  The convolution $\sigma * \sigma$ is supported on $\{ (\xi,\tau) \in \R^{d+1} : \tau \geq 2 \psi(\xi/2) \}$ and it achieves its maximum on the boundary. Moreover the inequality \eqref{eq:comprinc} is strict for almost every point in the interior of the support.
\end{remark}

\subsection{Optimal constant and nonexistence of extremizers}
Using the result by Foschi in the unperturbed case, we use an extremizing sequence that behaves like Gaussians, but are adapted to the perturbation. When such a sequence concentrate to a point in which the convolution $\sigma * \sigma$ equals the unperturbed one\footnote{this is a point in the boundary of the support of $\sigma * \sigma$ where the Hessian of $\phi$ vanishes.} we obtain the sharp constant $\mathcal C_\phi$.

\begin{teo}
  Let $\phi \in C^2(\R^2)$, nonnegative, strictly convex whose the Hessian $H(\phi)$ satisfies one of the following:
  \begin{enumerate}[(i)]
  \item $H(\phi)(y_0) = 0$ for some $y_0 \in \R^2$,
  \item exists a sequence \[ \{ y_n \}_{n \in \N} \subset \R^2, \, \abs{y_n} \underset{n \to \infty}{\longrightarrow} \infty \quad \text{ such that } \quad H(\phi)(y_n) \underset{n \to \infty}{\longrightarrow} \infty .\]
  \end{enumerate}

  Then the inequality \eqref{eq:perturbedStrichartz} holds for every $f \in L^2(\R^2)$ with the sharp constant
  \[ \mathcal C_\phi = \sqrt[4]{\frac \pi 2} .\]
\end{teo}

\fi

% \begin{define}
% A sequence $\{f_n\}_{n \in \N}$ in $\Ball(L^2)$ is an \emph{extremizing sequence} for \eqref{eq:strichartz} if
%   \[ \lVert e^{-it\Delta} f \rVert_{L_t^q(\R)L_x^p(\R^d)} \to \mathcal C  \qquad \text{ as } \, n \to \infty .\]
  
% \end{define}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../Thesis"
%%% End:
