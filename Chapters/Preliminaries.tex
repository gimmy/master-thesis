\chapter{Preliminaries and background}

\section{The Schrödinger equation}
The Schrödinger equation is a differential equation that describes the evolution of a quantum system.
It was introduced by Erwin Schrödinger in the 1925, who won the Nobel Prize for Physics eight years later.
The simplified form the of equation we use in this thesis is 
\begin{equation*}
  i \frac{\partial}{\partial t} \Psi(x,t) = \bra{\triangle + V(x,t)} \Psi(x,t)
\end{equation*}
where $i$ is the imaginary unit, % $\hbar = \frac{h}{2\pi}$ where $h$ is the Plank constant,
$\Psi$ is the so-called wave function and $V$ is a potential. \\

\noindent When the potential $V$ vanishes identically,
we have the initial value problem for the homogeneous Schrödinger equation
in $\R^d$ with initial datum $u_0$.
\begin{equation}
  \begin{cases} \label{eq:FreeSE} \tag{SE}
    i \partial_t u - \triangle u = 0 \\
    u(0,x) = u_0(x) % \,,\quad u_0 \in \Schwartz(\R^d).
  \end{cases}
\end{equation}

\subsection{Solution via Fourier analysis}
When the initial datum $u_0$ is  taken in the Schwartz space $\Schwartz(\R^d)$
% \begin{equation*}
%   \Schwartz(\R^d) = \set{f \in C^\infty(\R^d) \, : \, \sup_{x \in \R^d} \abs{x}^\alpha \abs{\partial^\beta f(x)} \text{ is finite} },
% \end{equation*}
it is possible to give an explicit formula for  
the solution of the initial value problem \eqref{eq:FreeSE},
by using the Fourier transform on $\R^d$.

\vspace{5mm}
\begin{minipage}{0.5\linewidth}
  \begin{center}
    Fourier transform
  \end{center}
  \begin{equation*}
    \label{eq:FourierTransform}
    \hat{f}(\xi) = \int_{\R^d} e^{-ix\cdot\xi} \, f(x) \d x ,
  \end{equation*}
\end{minipage}
\begin{minipage}{0.5\linewidth}
  \begin{center}
    Inverse Fourier transform
  \end{center}
  \begin{equation*}
    % \label{eq:InverseFourierTransform}
    \widecheck{f}(y) = \frac{1}{(2\pi)^d} \int_{\R^d} e^{iy\cdot\xi} \, f(\xi) \d \xi .
  \end{equation*}
\end{minipage} \vspace{2mm}

\noindent We will use the notation $\td \xi$ to indicate the normalised measure $\frac{\d \xi}{(2\pi)^d}$. With this normalisation the Fourier transform
is an isometry between $L^2(\R^d, \d x)$ and $L^2\pa{ \R^d, \td \xi}$. More results about the Fourier transform are recalled in the \cref{Appx:Fourier}.

\noindent We denote with $\Fourier_x$ the Fourier transform with respect to the space variable $x$, while $\Fourier$ or $\Fourier_{t,x}$ will stand for the space-time Fourier transform, in both time and space.

Applying the Fourier transform to \eqref{eq:FreeSE}, we get
\begin{equation*}
    \begin{cases}
    i \partial_t \hat{u} + \abs{\xi}^2 \hat{u} = 0 \\
    \hat{u}(0,x) = \hat{u_0}(x) \,,\quad \hat{u_0} \in \Schwartz(\R^d).
  \end{cases}
\end{equation*}
The first line is now an algebraic equation. Divide it by the imaginary unit: since $i^{-1} = - i$ we obtain
\begin{equation*}
  \partial_t \hat{u} - i \abs{\xi}^2 \hat{u} = 0 .
\end{equation*}
Multiplying by $e^{-it\abs{\xi}^2}$, we can rewrite the equation as
\begin{equation*}
  \partial_t(e^{-it\abs{\xi}^2} \hat{u}(t,\xi) ) = 0 . 
\end{equation*}
Now integrate from $0$ to $t$. Since $\lim_{t \to 0} e^{-it\abs{\xi}^2} \hat{u}(t,\xi) = \hat{u}(0,\xi)$, we have
\begin{equation*}
  \int_0^t \partial_\tau(e^{-i\tau\abs{\xi}^2} \hat{u}(\tau,\xi) ) \d \tau = e^{-it\abs{\xi}^2} \hat{u}(t,\xi) - \hat{u_0}(\xi) = 0 ,
\end{equation*} then
\begin{equation*}
  \hat{u}(t,\xi) =  e^{it\abs{\xi}^2} \, \hat{u}(0,\xi) .
\end{equation*}
The right hand side is still a Schwartz function, so we are allowed to take the Inverse Fourier transform in the space variable on both sides:
\begin{equation*}
  u(t,x) = \Fourier_x^{-1}\pa{e^{it\abs{\xi}^2} \, \hat{u}_0(\xi)}(x) = \int_{\R^d} e^{ix\cdot\xi} e^{it\abs{\xi}^2} \, \hat{u}_0(\xi) \td \xi .
\end{equation*}
Thus, our solution to \eqref{eq:FreeSE} is given by
\begin{equation}\label{eq:sol2SE}
  u(t,x) = e^{-it\Delta}u_0 ,
\end{equation}
where we indicated with $e^{-it\Delta}$ the evolution operator
\begin{equation*}
  (e^{-it\Delta} f)(t,x) := \Fourier_x^{-1}(e^{it\abs{\xi}^2}\hat{f}(\xi)) = \int_{\R^d} e^{i( x\cdot\xi + t\abs{\xi}^2 )} \, \hat{f}(\xi) \td \xi .
\end{equation*}

The solution \eqref{eq:sol2SE} enjoys the following symmetries.
\begin{itemize}\label{list of symmetries}
  \item space-time translations:
    $u(t, x) \rightsquigarrow u(t + t_0, x + x_0)$,
    with $t_0 \in \R$, $x_0 \in \R^n$;
  \item parabolic dilations:
    $u(t, x) \rightsquigarrow u(\lambda^2 t, \lambda x)$,
    with $\lambda > 0$;
  \item change of scale:
    $u(t, x) \rightsquigarrow \mu u(t, x)$, with $\mu > 0$;
  \item space rotations:
      $u(t, x) \rightsquigarrow u(t, R x)$, 
      with $R \in SO(n)$;
  \item phase shifts:
      $u(t, x) \rightsquigarrow e^{i \theta} u(t, x)$, 
      with $\theta \in \R$;
  \item Galilean transformations:
    \begin{equation*}
      u(t, x) \rightsquigarrow
      \exp\pa{\frac i4 \pa{\abs{v}^2 t + 2 v \cdot x}}
      u(t, x + t v),
    \end{equation*}
    with $v \in \R^n$.
  \end{itemize}
  Let $G$ be the group generated by the above symmetries, and let $g$ an element of $G$.  
  If $u$ solves~\eqref{eq:FreeSE} with initial data $u_0$, then $v = g \cdot u$ is still a solution to~\eqref{eq:FreeSE}, where $\cdot$ denotes the multiplication on the group $G$. 
  % Notice that the ratio $\norm{u}_{L^{p(n)}} / \norm{u(0)}_{L^2}$
  % is left unchanged by the action of $\mG$.

  Since we expressed the solution $u(t,x)$ to the Schrödinger equation \eqref{eq:sol2SE}
  via the (space-time) Fourier transform, we can use results from the realm of oscillatory
  integrals theory in order to study regularity of the solution.
% For the convenience of the reader, the main results are recalled in the Appendix.

  \section{Oscillatory integrals}
  Oscillatory integrals are one of the main tools in harmonic analysis
  since its very beginning.
  The Fourier transform is in fact an example of oscillatory integral.
  We recall a few important results about them.%that are used in this thesis.
    \begin{nota} If $x,y$ are real numbers, 
      we write $x = \bigO(y)$ or $x \lesssim y$ if there exists a finite positive constant $C$
      such that $\abs{x} \leq C \abs{y}$. We write $x \sim y$ if $C^{-1}\abs{y} \leq \abs{x} \leq C\abs{y}$
      for some $C \neq 0$.
    \end{nota}
% The proofs can be found in many books, for example in \cite[Chapter 8]{stein2011functional}.
The main contribution in the oscillatory integral comes from critical points of the phase: those points in which the gradient of phase $\nabla\varphi$ vanishes.
The following result is from \cite[Prop 2.1, Chapter 8, page 325]{stein2011functional}.
\begin{prop}[Principle of non-stationary phase]
  Let $\varphi \in C^\infty(\R^d), \psi \in C^\infty_c(\R^d)$, with $\abs{\nabla \varphi(x)} \geq c > 0$ for every $x \in \supp(\psi)$.
  Then for any $N \geq 0$
  \begin{equation*}
    \abs{I(\lambda)} = \Abs{\int_{\R^d} e^{i\lambda \varphi(x)} \psi(x) \d x} \leq c_N \lambda^{-N} \qquad \forall \, \lambda > 0,
  \end{equation*}
  where the constant $c_N$ depends also on $\varphi$ and $\psi$.
\end{prop}
When critical points are present, we cannot hope for such a decay.
\begin{ex}
  Consider the $1$-dimensional case: let $a<0<b$ and let $\psi$ be a smooth cut-off function supported in $\bra{-\frac \pi 2, \frac \pi 2}$ and focus on the real part:
  \begin{equation*}
    \Re\pa{  \int_a^b e^{i \lambda \varphi(x)} \psi(x) \d x  } = \int_a^b \cos(\lambda \varphi(x)) \psi(x) \d x \, .
  \end{equation*}
  As $\lambda$ gets larger, we get more cancellation when no critical points are present. Below we fix $\lambda = 100$ and we plot two example: in the first one on the left $\varphi(x) = x$, who does not have critical points; in the second $\varphi(x) = x^2$, and the derivative vanishes at the origin.
  
  \noindent \begin{minipage}{.5\linewidth}
    \begin{center}
    \begin{tikzpicture}[xscale=4]%\label{fig:varphi=x}
      \draw[->] (-pi/4 - .1 ,0) -- (pi/4 +.1 ,0) node[right] {};   % x
      \draw[->] (0,-1.2) -- (0,1.2) node[above] {};      % y

      % Cut-off
      \draw[blue] (0.4,1) node[above]{$\psi$};
      \draw[blue] (-.7,0) to[out=0,in=200] (-.1,.8);    
      \draw[blue] (-.1,.8) -- (.1,.8);
      \draw[blue] (.1,.8) to[out=-20,in=180] (.7,0);    

      % Oscillation
      \draw[samples=300,smooth,domain=-pi/4:pi/4] plot (\x, {cos((100*\x r))});
    \end{tikzpicture}
    \captionof{figure}{Plot of the function $\cos(100 x)$.}
  \end{center}
  \end{minipage}
  \begin{minipage}{.5\linewidth}
    \begin{center}
    \begin{tikzpicture}[xscale=4]%\label{fig:varphi=x2}
      \draw[->] (-pi/4 - .1 ,0) -- (pi/4 +.1 ,0) node[right] {};   % x
      \draw[->] (0,-1.2) -- (0,1.2) node[above] {};      % y

      % Cut-off
      \draw[blue] (0.4,1) node[above]{$\psi$};
      \draw[blue] (-.7,0) to[out=0,in=200] (-.1,.8);    
      \draw[blue] (-.1,.8) -- (.1,.8);
      \draw[blue] (.1,.8) to[out=-20,in=180] (.7,0);    

      % Oscillation
      \draw[samples=300,smooth,domain=-pi/4:pi/4] plot (\x, {cos((100*\x^2 r))});
    \end{tikzpicture}
    \captionof{figure}{Plot of the function $\cos(100 x^2)$.}
  \end{center}
  \end{minipage}
\end{ex} \vspace{2mm}

But we can still get some integrability if the critical points are not ``too critical''.
\begin{lemma}[van der Corput]
  Let $\varphi \in C^2[a,b]$ and $\abs{\varphi''(x)} \geq 1$ for all $x \in [a,b]$. Then
  \begin{equation*}
    \abs{I(\lambda)} = \Abs{\int_a^b e^{i \lambda \varphi(x)} \d x} \leq \frac{8}{\lambda^{\frac 12}} \qquad \forall \, \lambda > 0.
  \end{equation*}
\end{lemma}
\noindent The reader can find a proof in \cite[Prop 2.3, Chapter 8, page 328]{stein2011functional}

It is possible to partially generalize such result in higher dimension. See \cite{carbery2002van} for more details on what follows. 
\begin{define}
  Let $\varphi \in C^\infty(\R^d)$. A point $x_0 \in \R^d$ is a \emph{critical point} of $\varphi$ if $\nabla\varphi(x_0) = 0$.
  The point $x_0$ is \emph{non-degenerate} if the Hessian
  is non-degenerate in $x_0$, namely if
  \begin{equation*}
    \nabla^2\varphi (x_0) = \pa{\frac{\partial^2\varphi}{\partial x_i \partial x_j}(x_0)}_{i,j=1}^d \quad \text{has full rank}
  \end{equation*}
  or, equivalently, if $\det(\nabla^2\varphi(x_0)) \neq 0$.
\end{define}

\begin{teo}[%Principle of stationary phace -
  van der Corput in higher dimension \cite{carbery2002van}]\label{teo:higher_van_der_Corput}
  Let $\varphi \in C^\infty(\R^d), \psi \in C^\infty_c(\R^d)$, with $\det(\nabla^2 \varphi(x)) \neq 0$ for every $x \in \supp(\psi)$.
  Then
  \begin{equation*}
    I(\lambda) = \bigO(\lambda^{-\frac d2}) \, .
  \end{equation*}
\end{teo}

These results have important applications in the theory of restriction of the Fourier transform.

\section{Restriction theory}
Let $f$ be  an integrable function.
Then its Fourier transform $\hat{f}$ is a continuous function.
Thus, for every subset $E \subset \R^{d}$, the restriction of $\hat{f}$ to $E$ makes sense as a continuous function.
We can define a restriction operator $\mathcal R_E$
 which maps into the space $C(E)$ of continuous functions on $E$.
\begin{align*}
  \mathcal R_E \colon L^1(\R^{d}) & \to C(E) \\
  f & \mapsto \hat{f} \restriction_E \, .
\end{align*}
The operator $\mathcal R_E$ is a linear and bounded since
\begin{equation*}
   \norm{\hat{f} \restriction_E}_\infty \leq \norm{\hat{f}\,}_\infty = \sup_{\xi \in \R^d}\Abs{\int_{\R^d}e^{-ix\cdot\xi}f(x) \d x} \leq \int_{\R^d} \abs{f(x)} \d x = \norm{f}_{L^1} .
\end{equation*}

On the other hand, the restriction of an $L^2$ function
to a null set\footnote{when $E$ is a set of zero Lebesgue measure in  $\R^d$} makes no sense.

% the Fourier transform is an isometry on the space of square integrable functions:
% \begin{equation*}
%   \Fourier \colon L^2(\R^d) \to L^2(\R^d),
% \end{equation*}
% so the Fourier transform of a function in $L^2(\R^d)$ is just a function in $L^2(\R^d)$.

We wonder if it is possible to make sense to this operator on $L^p(\R^d)$, for $1 < p < 2$.
In other words, we wonder for which $(q,p)$ and $E\subset \R^d$ the operator
\begin{align*}
  \mathcal R_E \colon L^p(\R^{d}) & \to L^q(E) \\
  f & \mapsto \hat{f}\restriction_E
\end{align*}
is bounded, even when the subset $E$ has zero Lebesgue measure.\\
First, we start with defining a class of ``nice'' subsets on which we would like to restrict $\hat{f}$.  
We indicate with $(x^1,\dots,x^d)$ the coordinates of a vector $x \in \R^d$, and
with $(p^0, p^1,\dots,p^d)$ the coordinates of a point $p \in \R^{d+1}$. 
\begin{define}
  We say that $\M$ is a local smooth \emph{hypersurface} in $\R^{d+1}$ if it is locally a graph of a smooth map
  \begin{align*}
    \varphi \colon \R^{d} & \to \R \\
    x & \mapsto \varphi(x^1,\dots,x^d) . 
  \end{align*}
  % Hypersurfaces are, in particular, $d$-dimensional manifolds in $\R^{d+1}$.
  This means that for every point $p \in \M$ there exists a neighbourhood $M_p$ of $p$
  and a map $\varphi \in C^\infty(\R^d)$ such that
  \begin{equation*}
    \M_p = \set{(x^0,x) \in \R \times \R^d \, : \, x^0 = \varphi(x^1,\dots,x^d)}.
  \end{equation*}
\end{define}
\begin{ex}
  The $d$-dimensional sphere $\mathbb{S}^d = \set{x \in \R^{d+1} \, : \, \abs{x}^2 = 1}$ and the paraboloid $P = \set{(x,y) \in \R^{d} \times \R \, : \, y = \abs{x}^2}$ are hypersurfaces in $\R^{d+1}$.
\end{ex}

Let $\M$ be a smooth hypersurface in $\R^{d+1}$, and let $p \in \M$.
Then there exist an open neighbourhood $A$ of $p$, a point $x_0 \in \R^d$, an open neighbourhood $U$ of $x_0$ and map $\phi \in C^\infty(U)$ such that
\begin{equation*}
  \phi(x_0) = p \, , \qquad  A \cap \M = \phi(U) = \set{(x,y) \in U \times \R \, : \, y = \varphi(x^1,\dots,x^d)}.
\end{equation*}
The map $\phi(x) = (x,\varphi(x))$ (usually called ``chart'') maps $\phi \colon U \to \phi(U)$.
We can carry over the Lebesgue measure $\mathcal L^d$ on $\R^d$ to $\M$ via $\phi$.
Let $f$ be a smooth function supported in the compact region $V$ on $\M$, with $V = \phi(U)$.
We can define the surface measure $\sigma$ on the surface $\M$ via change of variables:
\begin{align*}
  \int_V f(v) \d \sigma(v) & := \int_{\phi(U)} f(v) \d \sigma(v) \\
  & = \int_U (f \circ \phi)(x) \, \abs{\Jac(\phi(x))} \d x =
  \int_U (f \circ \phi)(x) \sqrt{1 + \abs{\nabla\varphi(x)}^2} \d x .
\end{align*}
Up to translations and rotations, we can assume that $p = (x^0,x) = 0 \in \R^{d+1}$, $\phi(0)~=~(0,\varphi(0))~=~0$ and $\nabla\varphi(0) = 0$.
Once a basis of $\R^d$ is fixed, the Hessian of $\varphi$ at the origin can be represented as a matrix $\nabla^2\varphi(0)$.
This is a linear map on $\R^d$.
Since $\varphi$ is smooth, its Hessian is symmetric, so it has $d$ real eigenvalues $k_1,\dots,k_d$.
\begin{define}[Curvature]
  Let $p \in \M$ and $x \in \R^d$, with $\phi(x) = (x,\varphi(x)) = p$ as before.  
  The eigenvalues $k_1,\dots,k_d$ of the Hessian $\nabla^2\varphi(x)$ are called \emph{principal curvatures} of the $\M$ in $p$.
  The product of the eigenvalues
  \begin{equation*}
    k(p) := k_1 \cdot ... \cdot k_d = \det(\nabla^2\varphi(x))
  \end{equation*}
  is the \emph{Gaussian curvature} of $\M$ at $p$.
\end{define}


\noindent Let $\mu$ be a smooth compactly supported measure on the hypersurface $\M$.
For example, consider $\mu = \psi \sigma$, where $\sigma$ is the surface measure and $\psi \in C^\infty_c(\M)$, where $C^\infty_c(\M)$ is the space of smooth, compactly supported functions on the hypersurface $\M$.
Consider the Fourier transform of such a measure\footnote{A rigorous definition of Fourier transform of a measure is reported in \cref{sec:FTmeasure}.}.

When $\M$ has non vanishing Gaussian curvature at every point,
we can expect the Fourier transform of the measure $\mu$ to decay.
In fact, we assume that $\supp(\mu) = \phi(U)$ with $U$ compact, then
\begin{equation*}
  \hat{\mu}(\xi) = \int_{\phi(U)} e^{-i \xi \cdot y} \d \mu(y) = \int_U e^{-i \xi \cdot \phi(x)} \abs{\Jac(\phi(x))} \d x =
  \int_U e^{-i \xi \cdot \phi(x)} \pa{1 + \abs{\nabla\varphi(x)}^2}^{\frac12} \d x .
\end{equation*}
Expanding $\varphi(x)$ in the origin using Taylor, the first two terms are zero, so we have
\begin{equation*}
  \varphi(x) = \frac12 \sum_{j = 1}^{d} k_j x_j^2 + \bigO(\abs{x}^3).
\end{equation*}
When the principal curvatures $k_j$ never vanish, we can apply \cref{teo:higher_van_der_Corput} to get the decay
\begin{equation*}
  \hat{\mu}(\xi) = \bigO(\abs{\xi}^{-\frac{d}{2}}) .
\end{equation*}

\vspace{2mm}

We want to see if the same result holds true when we consider functions on $\M$.
Let us introduce an operator to deal with this case.
\begin{define}  Let $\M \subset \R^{d+1}$ be a $d$-dimensional manifold and $\mu$ a smooth measure supported on it.
  We define the following operators
          
  \noindent
  \begin{minipage}{0.45\linewidth}
      \begin{align*}
        \textit{Restriction } & \textit{ operator} \\
          \mathcal R \colon L^p(\R^{d+1}) & \to L^2(\M, \mu) \\
          f & \mapsto (\Fourier f)\restriction_\M
        \end{align*}
      \end{minipage}
      \begin{minipage}{0.45\linewidth}
        \begin{align}
          \textit{Extension } & \textit{ operator} \nonumber \\
          \mathcal R^\star \colon L^2(\M, \mu) & \to L^{p'}(\R^{d+1}) \label{eq:extension_operator} \\
          g & \mapsto \Fourier^{-1}(g \, \mu) \nonumber
        \end{align}
      \end{minipage}
      \vspace{4mm}      
    \end{define}
    \noindent The operator $\mathcal R^\star$ is the adjoint of $\mathcal R$.
    When the manifold $\M$ is curved and the measure $\mu$ is compactly supported
    (for example, consider the $d$-dimensional sphere $\Sphere^d$ with it surface measure $\sigma$),
    in the case $q=2$ one can use the following result due to Tomas and Stein \cite{tomas1975restriction}.
    \begin{teo}[Tomas-Stein]\label{Th:TS}
      Let $\Sphere^d \subset \R^{d+1}$,
      % $\M \subset \R^{d+1}$ a \emph{compact} $d$-dimensional manifold with non vanishing Gaussian curvature,
      and $f \in L^p(\R^{d+1})$, then
      \[ \norm{\mathcal R f}_{L^2(\Sphere^d)} \ls \norm{f}_{L^p(\R^{d+1})} \quad \text{ holds \, for } \quad 1 \le p \le \frac{2(d+2)}{d+4}. \]
    \end{teo}
    The dual statement for the extension operator reads:
    \begin{teo}[Dual Tomas-Stein]\label{Th:dualTS}
      Let $\Sphere^d \subset \R^{d+1}$,
      % Let $\M \subset \R^{d+1}$ a \emph{compact} $d$-dimensional manifold with non vanishing Gaussian curvature,
      and $g \in L^2(\Sphere^d)$, then
      \begin{equation}\label{dualTS}
        \norm{\mathcal R^\star g}_{L^{p'}(\R^{d+1})} \ls \norm{g}_{L^2(\Sphere^d)} \quad \text{ holds for } \quad p' \ge 2 + \frac 4 d. 
      \end{equation}
    \end{teo}
    \begin{remark}
      The implicit constants in \cref{Th:TS} and \cref{Th:dualTS} do not depend on the function $f$, nor on $g$. 
    \end{remark}
    
\section{Back to the Schrödinger equation}
Look closer at the solution of \eqref{eq:sol2SE}:
\[ u(t,x) = e^{-it\Delta}u_0 = \frac{1}{(2 \pi)^d} \int_{\R^d} e^{i(x \cdot \xi + t\abs{\xi}^2)}\widehat{u_0}(\xi) \d \xi .\]
We multiply and divide by $2\pi$ and we interpret the above formula as an inverse space-time Fourier Transform on $\R^{d+1}$:
\begin{equation*}
  u(t,x) = \int_{\R^d} e^{i(x \cdot \xi + t\abs{\xi}^2)}\widehat{u_0}(\xi) \td \xi =  \int_{\R^{d+1}} e^{i(t,x)\cdot(\tau,\xi)}  \, 2\pi \, \widehat{u_0}(\xi) \, \delta(\tau - \abs{\xi}^2) \frac{\d \tau}{2\pi} \td \xi
\end{equation*}
from which:
\begin{equation}
  u = \Fourier^{-1}(\underbrace{\widehat{u_0}(\xi)}_{f(\xi)} \, \underbrace{2 \pi \, \delta(\tau - \abs{\xi}^2)}_{\mu(\tau,\xi)}) = \Fourier^{-1}(f \, \mu).\label{eq:sol_as_restriction}
\end{equation}
where $\delta(\tau - \abs{\xi}^2)$ is a measure supported on the paraboloid $P = \{(\tau,\xi) \in \R^{d+1}, \tau~=~\abs{\xi}^2 \}$.\\

\noindent Thus, the solution of the Schrödinger equation \eqref{eq:FreeSE} is given by applying the extension operator $\mathcal R^\star$ to the function $\widehat{u_0}$ when $\M$ is the paraboloid $P$, and $\mu$ is the measure $2\pi\, \delta(\tau - \abs{\xi}^2)$.

\noindent The operator $e^{-it\Delta}$ is, in fact, the composition of $\mathcal R^\star$ with the spatial Fourier transform on $\R^d$. \vspace{-6mm}
  \begin{align*}
    u_0 & \xmapsto{\phantom{u_0} \hspace{7mm} e^{-it\Delta} \qquad} u(t,x) \\
    u_0 & \mapsto \widehat{u_0} \mapsto \mathcal R^\star\widehat{u_0} = u(t,x)     
  \end{align*}

\begin{remark}
  The Tomas-Stein inequality \eqref{dualTS} holds on \emph{compact} hypersurface. In the case of the paraboloid,  
  we can remove this assumption via rescaling\footnote{This is particular of the paraboloid, it is not possible to do it, for example, for the hyperboloid}.
  Consider $u_0 \in L^2(\R^d)$ such that
  \[ \supp(\widehat{u_0}) \subseteq \Ball_1^d = \{\xi \in \R^d \, : \, \abs{\xi} \leq 1\}. \]
  Rescaling $u_0$ with $\lambda > 0$, the Fourier transform changes with the dual scaling:
   \[ (u_0)_\lambda(x) = u_0(\lambda x) \quad \then \quad \widehat{(u_0)_\lambda}(\xi) =  \lambda^{-d}\widehat{u_0}(\xi /\lambda) = \widehat{u_0}^\lambda(\xi), \] then $ \widehat{u_0}^\lambda $ is supported on $\Ball_\lambda^d = \{\xi \in \R^d \, : \, \abs{\xi} \leq \lambda\}$ .
  The rescaled extension inequality %\eqref{dualTS}:
  \[ \norm{\mathcal R^\star \widehat{u_0}^\lambda}_{L^{p'}(\R^{d+1})} = \lambda^{-\frac{d+2}{p'}} \norm{\mathcal R^\star \widehat{u_0}}_{L^{p'}(\R^{d+1})} \le C \lambda^{-\frac d 2} \norm{\widehat{u_0}}_{L^2(\M)} =  \norm{\widehat{u_0}^\lambda}_{L^2(\M)} \]
  holds with constant $C_\lambda = C \lambda^{- \frac d 2 + \frac{d+2}{p'} }$.
  In particular, for the value $p' =  2 + \frac 4 d$ we have $C_\lambda = C$ for every $\lambda > 0$. 
  From \cref{Th:dualTS}, letting $\lambda \to \infty$ we obtain the bound for the whole paraboloid $P$.
  Since functions with compactly supported Fourier transform are dense in $L^2$, with a limiting argument we obtain the extension inequality for all initial data in $L^2$.
  %even if it is not a compact hypersurface, nor the measure on it has compactly supported density.
\end{remark}

Once we interpret the solution of the Schrödinger equation $e^{-it\Delta}u_0$ as
a Fourier extension from a paraboloid, we can apply \cref{Th:dualTS}, that holds,
by the previous remark, for $p(d) = 2 + \frac4d$. Then we have
\begin{equation}\label{TS to Schroedinger}
  \norm{e^{-it\Delta}u_0}_{L^{p(d)}_{t,x}(\R\times\R^{d})} \ls \norm{u_0}_{L^2(\R^d)} . % \quad \text{ holds for } \quad p' \ge 2 + \frac 4 d. 
\end{equation}
The exponent $p(d)$ is also called the \emph{Strichartz exponent}.

% In dimension $d=1,2$ the Strichartz exponent $p(d)$ is a even number, so % $6$ and $4$.
% we can exploit Plancherel \eqref{eq:Plancherel} in $L^2(\R^3)$. In dimension $d=2$ for example we have
% \begin{equation}
%   \label{eq:L4->L2}
%   \norm{ u }_{L^4(\R^3)} = %\sqrt{\norm{ \, u^2 }}_{L^2(\R^3)} =
%   \norm{ u \cdot  u }^{\frac 12}_{L^2(\R^3)} \overset{\eqref{eq:Plancherel}}{=} \norm{ \Fourier(u) * \Fourier(u)}^{\frac 1 2}_{L^2(\R^3, \td \xi)} \overset{\eqref{eq:sol_as_restriction}}{=} \norm{ f \mu * f \mu }^{\frac 12}_{L^2(\R^3, \td \xi)} .
% \end{equation}
% Then the problem of estimating the $L^4$-norm of the solution $u$ reduces to estimate the $L^2$-norm of the convolution of a measure with itself.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../Thesis"
%%% End:
