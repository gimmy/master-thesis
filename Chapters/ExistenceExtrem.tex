
\section{Existence of maximizers} % Extremizers}
In this section we prove the following theorem.
\begin{teo}\label{teo:existence}
  There exist a maximizer for the Strichartz inequality \eqref{eq:Strichartz}.
\end{teo}

Then, the best constant in  \eqref{eq:sharp_constant} is given by
\begin{equation*}
  \SharpC = \sup_{\substack{f \in L^2(\R), \\ f \neq 0}} \frac{ 6^{\frac16} \, \norm{\abs{\nabla}^{\frac13} e^{i t \triangle^2} f}_{L_{t,x}^6(\R \times \R)}}{\Norm{f}_{L^2(\R)}} = \max_{\substack{f \in L^2(\R), \\ f \neq 0}} \frac{ 6^{\frac16} \, \norm{\abs{\nabla}^{\frac13} e^{i t \triangle^2} f}_{L_{t,x}^6(\R \times \R)}}{\Norm{f}_{L^2(\R)}} .
\end{equation*}

\noindent In \cite[Theorem~1.8]{jiang2010linear} the authors proved a dichotomy result for extremizers of our Strichartz estimate:
\begin{teo}[Dichotomy, \cite{jiang2010linear}]
  Either
  \begin{itemize}
  \item[(i)] $\mathbf{S} = \Schr$ and there exist $f \in L^2(\R)$ and a sequence $\{a_n\}_{n \in \N}$ going to infinity as $n \to \infty$, such that $\{e^{ixa_n}f\}_{n \in \N}$ is an extremizing sequence for \eqref{eq:sharp_constant}, \emph{or}
  \item[(ii)] $\mathbf{S} \neq \Schr$ and extremizers for \eqref{eq:sharp_constant} exist. \label{th:to_show}
  \end{itemize}
\end{teo}

\noindent A solution to the extremizing problem  \eqref{eq:sharp_constant} is related to the one for the classical Schrödinger equation. We recall that the sharp constant for the Strichartz estimate for the free propagator $e^{-it\triangle}$ is
\begin{equation}\label{eq:sharpFS}
  \Schr := \sup_{v \in L^2(\R) \setminus \{ 0 \} } \frac{\norm{e^{-it\triangle}v}_{L_{t,x}^6(\R \times \R)} }{\norm{v}_{L^2(\R)}} = \pa{\frac{1}{12}}^{\frac{1}{12}} . % \sqrt[6]{\frac{\pi}{6\sqrt 3}} \approx 0.81923\dots.
\end{equation}
This constant was calculated by Foschi, see \cite[Theorem 1.1]{foschi2004maximizers}.

% \begin{remark}
%   Notice that $\mathbf{S} \geq \Schr$.
%   Intuitively, since we know that extremizers of \eqref{eq:sharpFS} are gaussians, we can consider a rescaled gaussian:
%   \begin{equation*}
%     g_{\lambda}(x) = e^{-\Abs{\frac x\lambda}^2} .
%   \end{equation*}
%   By the properties of the Fourier transform, we have that
%   \begin{equation*}
%     \hat{g_\lambda}(\xi) = \pa{g\pa{\frac{x}{\lambda}}}^{\hat{}}(\xi)= \lambda \, \hat{g}(\lambda \xi).
%   \end{equation*}
%   As $\lambda$ goes to $\infty$, $\hat{g_\lambda}$ concentrates near the origin\footnote{In fact, one has that
%     $ \hat{g_\lambda} \to \delta_0 $ 
%   in the sense of distributions, as $\lambda \to \infty$.}
%   The parabola $y= x^2$ and the quartic $y = x^4$ are close to each other for $x$ near to the origin.
%   In particular for large $\lambda$, $w(\frac{\eta}{\lambda}) = \Abs{\frac{\eta}{\lambda}}^{\frac 23} \leq 1$, and
%   \begin{equation*}
%     \int e^{i t \pa{\frac{\eta}{\lambda}}^4 + ix \frac{\eta}{\lambda}} w(\eta/\lambda) \hat{g}(\eta) \d \eta \approx \int e^{i t \pa{\frac{\eta}{\lambda}}^2 + ix \frac{\eta}{\lambda}} \hat{g}(\eta) \d \eta . 
%   \end{equation*}
%   Furthermore, we know that for any $\lambda > 0$, $g_\lambda$ realises the maximum in \eqref{eq:sharpFS}, so
%   \begin{equation*}
%     \hspace{-2mm} \SharpC = \sup_{f \in L^2} \frac{6^{\frac 16} \norm{\abs{\nabla}^{\frac13} e^{i t \triangle^2} f}_{L_{t,x}^6}}{\norm{f}_2} \geq \frac{6^{\frac 16} \norm{\abs{\nabla}^{\frac13} e^{it\triangle^2}g_\lambda}_{L_{t,x}^6}}{\norm{g_\lambda}_2} \geq \frac{\norm{\abs{\nabla}^{\frac13} e^{it\triangle^2}g_\lambda}_{L_{t,x}^6}}{\norm{g_\lambda}_2} \approx \frac{\norm{e^{-it\triangle}g_\lambda}_{L_{t,x}^6}}{\norm{g_\lambda}_2} = \Schr.
%   \end{equation*}
% \end{remark}
To prove the existence of extremizers it is enough to show a lower bound for $\SharpC$ good enough to ensure that
\begin{equation}
  \SharpC > \Schr .\label{eq:toshow}
\end{equation}
This will rule out the first case in \cref{th:to_show}.
The condition \eqref{eq:toshow} is equivalent to
\begin{equation*}
  \frac{\pi}{3}\SharpC^6 > \frac{\pi}{3}(\Schr)^6 = \frac{\pi}{6\sqrt{3}} \, ,
\end{equation*}
and because of \eqref{eq:sharpCequiv},
it is also equivalent to
  \begin{equation}\label{eq:aim:beat the constant}
    \sup_{\substack{f \in L^2(\R), \\ f \neq 0}} \frac{\norm{f \sqrt w \nu * f \sqrt w \nu * f \sqrt w \nu}^2_{L^2(\R^2)} }{\norm{f}^6_{L^2(\R)}} > \frac{\pi}{6\sqrt{3}} .
  \end{equation}
We can approximate the left hand side with some explicit function $f \in L^2$.

  \subsection{Lower bound for the sharp constant $\SharpC$}
  We start proving a result analogous to the \cite[Lemma~6.1]{quilodran2016extremizers} adapted for the $3$-fold convolution where the perturbation is $\Psi(x) = x^4$.
  \begin{lemma} \label{lm:sloppy_lower_bound}
    Consider the measure $\nu(y,t) = \ddelta{t-y^4} \d y \d t$. Let $E$ denote the support of the convolution measure $\nu*\nu*\nu$.
    Let $w(x)$ be the non-negative function $\abs{x}^{\frac23}$ and consider $f(x) = e^{-x^4}\sqrt{w(x)} \in L^2(\R)$, then the convolution $f \sqrt{w} \, \nu * f \sqrt{w} \, \nu * f \sqrt{w} \, \nu  \in L^2(\R^2)$ and the following lower bound holds:
    \begin{equation}
      \label{eq:sloppy_lower_bound}
      \frac{\norm{f \sqrt{w} \, \nu * f \sqrt{w} \, \nu * f \sqrt{w} \, \nu }^2_{L^2(\R^2)} }{\norm{f}^6_{L^2(\R)}} \geq \frac{ \norm{f}^6_{L^2(\R)} }{\int_E e^{-2\tau} \d\tau \d\xi}.
    \end{equation}
  \end{lemma}

  \begin{proof}
    The following identity holds:
    \begin{equation}
      f^2 \nu * f^2 \nu * f^2 \nu (\xi,\tau) = e^{-\tau} \; f \sqrt w \nu * f \sqrt w \nu * f \sqrt w \nu (\xi,\tau).     \label{eq:identities_for_f_2}
    \end{equation}
    Moreover we have
    \begin{equation}
      \label{eq:convolution_is_actually_norm}
      \int_{\R^2} f^2 \nu * f^2 \nu * f^2 \nu (\xi,\tau) \d\xi\d\tau = \norm{f}_{L^2(\R)}^6.
    \end{equation}
    Then using the preceding identities and Cauchy-Schwarz we obtain
    \begin{align*}
      \norm{f}_{L^2(\R)}^6 & = \int_{\R^2} e^{-\tau} ( f \sqrt w \nu * f \sqrt w \nu * f \sqrt w \nu)(\xi,\tau) \d\tau \d\xi \\
                        & \leq \pa{\int_{E} e^{-2\tau} \d\tau\d\xi}^{\frac12} \, \norm{f \sqrt w \nu * f \sqrt w \nu * f \sqrt w \nu}_{L^2(\R^2)}.
    \end{align*}
    that implies the desired inequality \eqref{eq:sloppy_lower_bound}.
  \end{proof}

  We can explicitly calculate the right hand side of \eqref{eq:sloppy_lower_bound}.
  The function $f(x)~=~e^{-x^4}\sqrt{w(x)}$ is even, and we have
  \begin{equation*}
    \norm{f}_{L^2(\R)}^2 = \int_{\R} e^{-2x^4} (x^2)^{\frac13} \d x = \frac12 \int_0^\infty e^{-2z} z^{\frac{5}{12} -1} \d z = \frac12 \frac{\Gamma(\frac{5}{12})}{2^{\frac{5}{12}}}, \qquad \text{ so } \quad\norm{f}_{L^2}^6 = \frac{1}{2^4} \frac{\Gamma(\frac{5}{12})^3}{2^\frac14}.
  \end{equation*}
  We compute the denominator. The support of $\nu*\nu*\nu$ is $E = \set{(\xi,\tau) \in \R^2 : \tau \geq \frac{\xi^4}{27}}$, see \cref{prop:measure symmetries}.
  \begin{equation*}
    \displaystyle  \int_E e^{-2\tau} \d\tau\d\xi = \int_\R \int_{\frac{1}{27}}^\infty e^{-2 \lambda \xi^4} \xi^4 \d\lambda\d\xi = \frac12 \int_{\frac{1}{27}}^\infty \pa{ \int_0^\infty e^{-2u} u^{\frac54 -1} \d u} \frac{\d\lambda}{\lambda^{\frac14 + 1}} 
    = \frac{3^{\frac34} \, \Gamma(\frac54)}{2^{\frac14}}.
  \end{equation*}
  
  Putting all together, we obtain the lower bound:
  \begin{equation} \label{eq:first_approximation}
    \frac{ \norm{f}^6_{L^2(\R)} }{\int_E e^{-2\tau} \d\tau \d\xi} = 
    \frac{1}{2^{4}3^{\frac34}} \, \frac{\Gamma(\frac{5}{12})^3}{\Gamma(\frac54)} \approx 0.2913141\dots
  \end{equation}
  Unfortunately, this quantity is not large enough to defeat the sharp constant, in fact:
  \begin{equation*}
    \frac{\pi}{3}(\Schr)^6 = \frac{\pi}{6\sqrt3} \approx 0.302299 > 0.2913141.
  \end{equation*}


\subsection{Improved lower bound}
The inequality in \cref{lm:sloppy_lower_bound} was too crude and we need a new way to approximate $\norm{f \sqrt w \nu * f \sqrt w \nu * f \sqrt w \nu}^2_{L^2(\R^2)}$.
For this purpose, we exploit the identities \eqref{eq:identities_for_f_1}, \eqref{eq:identities_for_f_2}, and the following properties
of the convolution measure.
\begin{prop}\label{prop:measure symmetries}
  Let $w(\xi) =\abs{\xi}^{\frac{2}{3}}$ and $\nu$ be the measure defined by
  \begin{equation*}
    \nu(\xi, \tau) = \ddelta{\tau - \abs{\xi}^4} \d\xi \d\tau .
  \end{equation*}
  Then the following properties hold for the convolution measure $w\nu *w\nu *w\nu$.
  \begin{enumerate}%[(a)]
  \item[(a)] It is absolutely continuous with respect the Lebesgue measure on $\R^2$.
  \item[(b)] Its support is given by
    \[ E = \{ (\xi,\tau) \in \R^2 \, : \, \tau \geq 3^{-3} \abs{\xi}^4 \} .\]
    % \item Its Radon-Nikodym derivative, also denoted by $w\nu_p * w\nu_p * w\nu_p$, defines a bounded function in the interior of the set $E_p$. 
  \item[(c)] It is \emph{radial} and \emph{homogeneous} of degree zero in $\xi$, the sense that:
    \begin{equation*}
      (w\nu *w\nu *w\nu)(\lambda \xi, \lambda^4 \tau) =  (w\nu *w\nu *w\nu)(\xi,\tau), \quad \text{ for every } \lambda > 0.
    \end{equation*}
    and \, $(w\nu * w\nu * w\nu)(- \xi,\tau) = (w\nu * w\nu * w\nu)(\xi,\tau) \quad$ for every $\xi \in \R$.
  \end{enumerate}
\end{prop}
\vspace{2mm}

Following the work of Oliveira e Silva e Quilodrán in \cite[Proposition~6.4]{quilodran2016extremizers}
we will prove a more general result.
The proof of the previous Proposition will follow from this taking the power $p=4$.
\begin{prop}
  Given $p \geq 2$, let $w(\xi) =\abs{\xi}^{\frac{p-2}{3}}$. Let $\nu_p$ be the measure defined by
  \begin{equation*}
    \nu_p(\xi, \tau) = \delta(\tau - \abs{\xi}^p) \d \xi \d t .
  \end{equation*}
  The following assertions hold
    Then the following properties hold for the convolution measure $w\nu_p * w\nu_p * w\nu_p$.
  \begin{enumerate}%[(a)]
  \item[(a)] It is absolutely continuous with respect the Lebesgue measure on $\R^2$.
  \item[(b)] Its support is given by
    \[ E_p = \{ (\xi,\tau) \in \R^2 \, : \, \tau \geq 3^{1-p} \abs{\xi}^p \} .\]
  % \item Its Radon-Nikodym derivative, also denoted by $w\nu_p * w\nu_p * w\nu_p$, defines a bounded function in the interior of the set $E_p$. 
  \item[(c)] It is \emph{radial} in $\xi$, and \emph{homogeneous} of degree zero in $\xi$, in the sense that:
    \[ (w\nu_p * w\nu_p * w\nu_p)(\lambda \xi,\lambda^p \tau) = (w\nu_p * w\nu_p * w\nu_p)(\xi,\tau), \quad \text{for every } \lambda > 0,\]
    and \, $(w\nu_p * w\nu_p * w\nu_p)(- \xi,\tau) = (w\nu_p * w\nu_p * w\nu_p)(\xi,\tau) \quad$ for every $\xi \in \R$.
  \end{enumerate}
\end{prop}

\begin{proof}
  With the change of variables: $\eta \mapsto \frac 23 \xi + \eta$ , $\zeta \mapsto\frac{\xi}{3} - \zeta$,
  we can write the convolution measure as
  \begin{equation*}
    \label{eq:convolution}
    (w\nu_p * w\nu_p * w\nu_p)(\xi,\tau) = \iint_{\R_\eta \times \R_\zeta} A_{\xi,\tau}(\eta,\zeta) \d\eta \d\zeta ,
  \end{equation*}
  where
  \begin{equation*}
    A_{\xi,\tau}(\eta,\zeta) := {\textstyle \delta(\tau - \abs{\frac{\xi}{3}- \eta}^p - \abs{\frac{\xi}{3}- \zeta}^p - \abs{\frac{\xi}{3}+\eta+\zeta}^p) \left(\abs{\frac{\xi}{3}- \eta}  \, \abs{\frac{\xi}{3}- \zeta} \, \abs{\frac{\xi}{3}+\eta+\zeta}\right)^{\frac{p-2}{3}} }.                         
  \end{equation*}
  \begin{itemize}
  \item[(a),(c)] It is enough to change variables and use that $\delta(\lambda^p F(x) ) = \lambda^{-p}\delta(F(x))$ (see \cite[Appendix]{foschi2017some}).
    Note also that
    \begin{align*}
      (w\nu_p * w\nu_p * w\nu_p)(-\xi,\tau) & = \iint A_{-\xi,\tau}(\eta,\zeta) \d\eta \d\zeta \\
      & =  \iint A_{\xi,\tau}(-\eta,-\zeta) \d\eta \d\zeta = (w\nu_p * w\nu_p * w\nu_p)(\xi,\tau).
    \end{align*}
  \item[(b)] First we show that every point in $E_p$ belongs to the support of $w\nu_p * w\nu_p * w\nu_p$.
    In fact, let $\psi(y)=\abs{y}^p$ and consider $y_1, y_2, y_3 \in \R$ such that
    \[ \xi = y_1+y_2+y_3, \quad  \tau = \psi(y_1) + \psi(y_2) + \psi(y_3) .\]
    From the midpoint convexity of $\psi$ it follows:
    \begin{equation}
      \label{eq:convexity}
      \frac 13 \tau = \frac 13 \psi(y_1) + \frac 13 \psi(y_2) + \frac 13 \psi(y_3) \geq \psi\left( \frac{y_1 + y_2 + y_3}{3} \right) = \psi(\xi/3).
    \end{equation}
    On the other hand, consider $(\xi,\tau) \in \R^2$ such that $\tau \geq 3 \psi(\xi/3)$. We want to find $y_1, y_2, y_3 \in \R$ as before.
    It is enough to find $y_1, y_2$, since $y_3 = \xi - (y_1 + y_2)$. The left hand side of \cref{eq:convexity} is convex and continuous, and it goes to infinity as $\abs{(y_1,y_2)} \to \infty$. Then, for every fixed $\tau \geq 3 \psi(\xi/3)$, for the Intermediate Values Theorem, there exists $(y_1,y_2) \in \R^2$ such that
    \begin{equation*}
      \tau = \psi(y_1) + \psi(y_2) + \psi(\xi - (y_1+y_2)) \geq 3 \psi(\xi/3).
    \end{equation*}

   \iffalse                     % skip this part, to fix
  \item[(c)] Because of (d), we can assume $\xi = 3$, then $\tau = 3^p \lambda$ and we can rewrite the convolution measure %\eqref{eq:convolution}
    as    
    \begin{equation*}
      \hspace{-1.5cm}   (w\nu_p * w\nu_p * w\nu_p)(3,3^p \lambda) = \iint_{\R^2} {\textstyle \delta(3^p \lambda - \abs{1- \eta}^p - \abs{1-\zeta}^p - \abs{1+\eta+\zeta}^p) (\abs{1- \eta}^2 \, \abs{1-\zeta}^2 \, \abs{1+\eta+\zeta}^2)^{\frac{p-2}{6}} } \d \eta \d \zeta .
    \end{equation*}
    Consider $v := (\eta,\zeta)$ as a vector in $\R^2$, we can use polar coordinate % such that    
    % \[
    %   \ang{v,(\sgn(\xi),0)} = \eta, \quad \text{ and so let be } \quad      
    %   \begin{dcases}
    %     \eta  & = r \cos \theta \\
    %     \zeta & = r \sin \theta .
    %   \end{dcases} \]
    we get
    \begin{equation}
      \label{eq:polar}
      (w\nu_p * w\nu_p * w\nu_p)(3,3^p \lambda) = \int_0^{2\pi} \int_{0}^\infty {\textstyle \delta(3^p \lambda - 3 - \varphi_{\theta}(r) ) } \, \mathds W(\theta, r) \d r \d \theta .
    \end{equation}
    The function $\varphi_\theta(r)$ given by
    \begin{equation*}
      \varphi_\theta (r) = \pa{(r\cos\theta -1)^2}^{\frac p2} + \pa{(r\sin\theta-1)^2}^{\frac p2} + \pa{(1+ r(\cos\theta+\sin\theta))^2}^{\frac p2} -3
    \end{equation*}
    % \begin{eqnarray*}
    %   \varphi_\theta (r) & = & (1- 2r \cos \theta + r^2 \cos^2 \theta)^{\frac p2} + (1-2r \sin\theta + r^2 \sin^2 \theta)^{\frac p2} \\
    %                     & + & (1+ 2r(\cos\theta+\sin\theta) + r^2(\sin\theta + \cos\theta)^2)^{\frac p2} -3
    % \end{eqnarray*}
    is convex, because sum of convex functions, with unique global minimum at $r=0$.
    While the product of weights $\mathds W(\theta,r)$ is given by
    \begin{equation*}
      \mathds W(\theta, r) = \pa{ (r \cos \theta -1)^2 (r \sin\theta -1)^2 (1+r\cos\theta+r\sin\theta)^2 }^{\frac{p-2}{6}} r .
    \end{equation*}
    The derivative $\varphi_\theta'(r)$ is given by
%    \begin{eqnarray*}
%      \varphi_\theta'(r) =&  & p \cos \theta (r \cos \theta -1)(1-2r\cos\theta+r^2 \cos^2\theta)^{\frac{p-2}{2}} \\ 
%                          &  +  & p \sin\theta (r \sin\theta -1)(1-2r\sin\theta + r^2 \sin^2\theta)^{\frac{p-2}{2}} \\
%                          &  +  & p (r + \sin\theta + \cos\theta) (1+2r(\sin\theta + \cos\theta) + r^2)^{\frac{p-2}{2}}.
%    \end{eqnarray*}
%    We can collect some term obtaining:
    \begin{eqnarray*}
      \varphi_\theta'(r) & = & p \cos \theta \pa{(r \cos \theta -1)^2}^{\frac p2} + p \sin\theta \pa{(r \sin\theta -1)^2}^{\frac p2} \\
                          &  +  & p (\sin\theta + \cos\theta) \pa{(1 + r\sin \theta + r\cos \theta)^2}^{\frac{p}{2}} .
    \end{eqnarray*}
    % Notice that when $r=0$ we have
    % \begin{equation*}
    %   \varphi_\theta'(0) =  p (\cos\theta +\sin\theta)(-1)^{p-1} + p (\sin\theta + \cos\theta) = 0 \quad \text{ for even } p.
    % \end{equation*}

    % TODO: check if this is the only zero of $\varphi_\theta'$ in this case. (Numerically checked for $p=4$).

    The change of variables $s = \varphi_\theta(r)$ yields
    \begin{align*}    
      (w\nu_p * w\nu_p * w\nu_p)(3,3^p \lambda) = & \int_0^{2\pi} \int_{0}^\infty \delta(3^p \lambda - 3 - s ) \, \frac{\mathds{W}(\theta, r)}{\varphi_\theta'(r)} \d s \d \theta \\
      =  & \, \1{\set{\lambda \geq 3^{1-p}}}(\lambda) \int_0^{2\pi} \frac{\mathds{W}(\theta, r)}{\varphi_\theta'(r)} \d \theta.
    \end{align*}
    where $r = \varphi_\theta^{-1}(3^p\lambda - 3)$. Since $\varphi_\theta$ is non-negative, convex and continuous, the inverse map
    \begin{equation*}
      \varphi_\theta^{-1} \colon \R^+ \to \R^+
    \end{equation*}
    is well-defined. % Todo{(to finish)}

    \fi
    
  \end{itemize}

\end{proof}

We have that
\begin{equation*}
  \norm{f \sqrt w \nu * f \sqrt w \nu * f \sqrt w \nu}^2_{L^2(\R^2)} = \int_{\R^2} e^{-2\tau} (w\nu *w\nu *w\nu)^2(\xi,\tau) \d\xi \d\tau ,
\end{equation*}
because, in the support of the measure, we can write $\tau = \lambda \xi^4$, for $\lambda \geq 3^{-3}$, and the following equality holds
\begin{equation}
  (f \sqrt w \nu * f \sqrt w \nu * f \sqrt w \nu )(\xi,\tau) = e^{-\tau} \; (w \nu * w \nu * w \nu) (\xi,\tau).     \label{eq:identities_for_f_1}
\end{equation}

Applying  Fubini and the change of variables $\lambda \xi^4 = u$, we get 
\begin{align*}
  \int_{\R^2} e^{-2\tau} (w\nu *w\nu *w\nu)^2(\xi,\tau) \d\xi \d\tau & = \int_{\frac{1}{27}}^\infty (w\nu *w\nu *w\nu)^2(1,\lambda) \, 2 \int_0^\infty e^{-u} \pa{\frac{u}{\lambda}}^{\frac54 -1} \frac{\d u}{4 \lambda} \, \d\lambda \\
  & = \frac{\Gamma\pa{\frac54}}{2^\frac14} \int_0^{3^{\frac34}} (w\nu *w\nu *w\nu)^2(1,t^{-4}) \d t .
\end{align*}

\noindent \begin{minipage}{0.4\linewidth}
  \begin{remark}
    As we saw in \cref{prop:measure symmetries}
    the value of the convolution measure depends only on one parameter.
    This because $\nu*\nu*\nu$ is radial and it is constant along 
    branches of the quartic $\tau = \alpha \xi^4$.
    Let $\alpha(t) = t^{-4}$ the amplitude of the quartic $\tau = \alpha(t) \xi^4$.
    When $t$ ranges in $(0,3^{\frac34}]$, $\alpha(t)$ gives all possible amplitudes of quartic in the support of $\nu*\nu*\nu$.
  \end{remark}
\end{minipage}\hspace{1mm}
\begin{minipage}{0.6\linewidth}
  \begin{tikzpicture}
    \begin{scope}
      % \draw  (-5,0) -- (5,0) arc(0:180:5) --cycle;
      \clip (-5,0) -- (5,0) arc(0:180:5) --cycle;
      % base of the support
   \draw[scale=2,domain=-3:3,smooth,variable=\x,black,fill=lightgray!40] plot ({\x},{.037*(\x)^4});

      % sum of quartic
      \draw[scale=2,dashed,domain=-2:2,smooth,variable=\x,black] plot ({\x},{(\x)^4});
      \draw[scale=2,dashed,domain=-2:1,smooth,variable=\x,black] plot ({\x},{(\x+.6)^4+.1});
      \draw[scale=2,dashed,domain=-4:0,smooth,variable=\x] plot ({\x},{(\x+1.3)^4+.4});
    \end{scope}
    
    % Different amplitudes on the right
    \begin{scope}    
      \clip (-4.5,0) -- (4.5,0) arc(0:180:4.5) --cycle;
      \foreach \a in {.08,.2,.6,7}
      \draw[purple,scale=2,domain=0:2,smooth,variable=\x] plot ({\x},{\a*(\x)^4});
    \end{scope}

    \draw[->] (-4,0) -- (4,0) node[right] {$\xi$};   % x 
    \draw[->] (0,0) -- (0,4) node[above] {$\tau$};        % y

    \draw[purple] (3.6,.7) node[below]{$\alpha(t)$};
    \draw[purple, dotted, thick, ->] (3.4,.7) arc (40:85:4.5) ;

    % \draw (-2.5,4.3) node[above]{$\{ \tau \geq 3^{-3} \abs{\xi}^4 \}$};
  \end{tikzpicture}
  \captionof{figure}{Support of the measure $w\nu * w\nu * w\nu$. Its value in a point $(\xi,\tau)$ depends only on $\alpha(t)$.}
\end{minipage}\vspace{4mm}

After rescaling, since the integrand is even, we can write
\begin{equation*}
  \int_0^{3^{\frac34}} (w\nu *w\nu *w\nu)^2(1,t^{-4}) \d t = 3^\frac34 \cdot \frac12 \int_{-1}^1 g^2(t) \d t
\end{equation*}
where the function of the right hand side is $g(t) = (w\nu *w\nu *w\nu)(1,3^{-3}t^{-4})$. We have
\begin{equation} \label{eq:quantity_with_coefficients}
  \frac{\norm{f \sqrt w \nu * f \sqrt w \nu * f \sqrt w \nu}^2_{L^2(\R^2)}}{\norm{f}_{L^2(\R)}^6} = \pa{\frac{2^4 3^\frac34  \Gamma\pa{\frac54}}{\Gamma\pa{\frac{5}{12}}^3}} \, \dashint_{-1}^1 g^2(t) \d t .
\end{equation}
Thus, computing a lower bound for the numerator in the left hand side it reduces to approximate the norm of $g$ in $L^2([-1,1])$ equipped with the normalised scalar product $\nang{f,g} = \dashint_{-1}^1 f g$.
With this purpose in mind, we consider the set of all monomials $\set{1,t,t^2,\dots}$. It is a complete system in $L^2([-1,1])$.
Using the Gram-Schmidt process we obtain a well-known orthogonal basis: the Legendre polynomials.
\begin{define}[Legendre polynomials]
  Let denote with $P_n = P_n(t)$ the solution to the differential equation:
  \begin{equation*}
    {d \over dx} \left[ (1-x^2) {d \over dx} P_n(x) \right] + n(n+1)P_n(x) = 0.
  \end{equation*}
  The function $P_n(t)$ is the $n^{\text{th}}$ Legendre polynomial.
  The set $\set{P_n}_{n \in \N}$ is an orthogonal basis of $L^2([-1,1])$.

  The first four even Legendre polynomials are plotted below.
  
  \noindent \begin{minipage}{0.55\linewidth}
    % Legendre polynomials
    \centering
    \begin{tikzpicture}[xscale=3,yscale=2]
      \draw[lightgray,->] (-1,0) -- (1.1,0) node[right] {};   % x 
      \draw[lightgray,->] (0,-.8) -- (0,1.2) node[above] {};    % y

      \draw[lightgray, step=.5cm] (-1,-1) grid (1,1);
      % \draw (-1.1,-1.2) rectangle (1.1,1.3);
      % \draw (1,-1.2) node[above] {$1$};
      
      \clip (-1,-1) rectangle (1,1);
      \draw[teal,thick,domain=-1:1,smooth,variable=\x] plot ({\x},{1});     
      % \draw[teal,domain=-1:1,smooth,variable=\x] plot ({\x},{\x});
      \draw[cyan,thick,domain=-1:1,smooth,variable=\x] plot ({\x},{(1.5)*(\x)^2 - 0.5});
      \draw[blue,thick,domain=-1:1,smooth,variable=\x] plot ({\x},{(0.125)*(35*(\x)^4 - 30*(\x)^2 +3)});
      % \draw[red,domain=-1:1,smooth,variable=\x] plot ({\x},{(0.125)*(63*(\x)^5 - 70*(\x)^3 + 15*(\x))});
      \draw[magenta,thick,domain=-1:1,smooth,variable=\x] plot ({\x},{(0.0625)*(231*(\x)^6 - 315*(\x)^4 + 105*(\x)^2 - 5)});      
    \end{tikzpicture}
    \captionof{figure}{First four even Legendre polynomials.}
  \end{minipage}%\hspace{.5cm}  
  \begin{minipage}{0.5\linewidth}
    \vspace{-1cm}
    \begin{align*}
      \begin{tikzpicture} \draw [teal, line width=3]
        (0,0) -- (.5,0); \end{tikzpicture} \, P_0(t) &= 1 , \\
      % {\color{teal}-} \, P_1(t) &= t, \\
      \begin{tikzpicture} \draw [cyan, line width=3]
        (0,0) -- (.5,0); \end{tikzpicture} \, P_2(t) &= \textstyle \frac{1}{2} \, (3 t^2-1), \\
      % {\color{magenta}-} \, P_3(t) &= \textstyle \frac{1}{2} \, (5 t^3-3 t) , \\
            \begin{tikzpicture} \draw [blue, line width=3]
        (0,0) -- (.5,0); \end{tikzpicture} \, P_4(t) &=  \textstyle \frac18 \, (35 t^4 - 30 t^2 +3), \\
      % {\color{purple}-} \, P_5(t) &=  \textstyle \frac18 \, (63 t^5 - 70 t^3 + 15t).\\
      \begin{tikzpicture} \draw [purple, line width=3]
        (0,0) -- (.5,0); \end{tikzpicture} \, P_6(t) &=  \textstyle \frac1{16} \, (231 t^6 - 315 t^4 + 105 t^2 - 5).                    
    \end{align*}
  \end{minipage} \vspace{4mm}
  
  Moreover, with the normalised scalar product, we have that
    \begin{equation*}
      \nang{P_m,P_n} = \dashint_{-1}^1 P_m(t) P_n(t) \d t = \frac{1}{2n+1} \delta_{m,n} ,
    \end{equation*}
    thus $\nnorm{P_n}^2 = (2n+1)^{-1}$.
    We indicate with $Q_n$ the normalised polynomial $\frac{P_n}{\nnorm{P_n}}$. Then $Q_n = (\sqrt{2n+1})\, P_n$ and $\set{Q_n}_{n \in \N}$ is an orthonormal basis of $L^2([-1,1], \nang{\cdot,\cdot})$.
\end{define}


We are now ready to prove the existence of maximizers.
\begin{proof}[Proof of \cref{teo:existence}]
  The norm of $g$ can be written as
  \begin{equation*}
    \nnorm{g}_{L^2}^2 = \sum_{n \geq 0} \nang{g,Q_n}^2 = \sum_{n \geq 0} \nang{g,Q_{2n}}^2, % = \sum_{n \geq 0} (c_{2n})^2 ,
  \end{equation*}
  since the function $g$ is even. We can approximate the norm calculating arbitrarily many coefficients:
  \begin{equation*}
    c_n^2 :=  \nang{g,Q_n}^2  =  (2n+1) \, \pa{\dashint_{-1}^1 g(t) P_n(t) \d t}^2 .
  \end{equation*}
  
  These coefficients can be retrieved from the \emph{moments} of the measure $g$:
  \begin{equation*}
    \mathcal I_k := \int_{-1}^1 g(t) \, t^k \d t, 
  \end{equation*}
  once we have them, one can obtain the value of $c_n$.

% \subsection{Computing moments}

  To compute $\mathcal I_k$ we consider the function $f(x) \sqrt{w(x)} e^{bx} =: h_b^2(x)$. \\
  In view of \eqref{eq:identities_for_f_2} and \eqref{eq:convolution_is_actually_norm} we have
\begin{align*}
  \int_{\R^2}( h_b^2 \nu * h_b^2 \nu * h_b^2 \nu)(\xi,\tau) \d\xi\d\tau & = \pa{\norm{h_b}_{L^2(\R)}^2}^3 =: G(b)^3 \\
  \int_{\R^2}( h_b^2 \nu * h_b^2 \nu * h_b^2 \nu)(\xi,\tau) \d\xi\d\tau & =   \int_{\R^2} e^{-(\tau-b\xi)}( w \nu * w \nu * w \nu)(\xi,\tau) \d\xi\d\tau =: F(b).
\end{align*}
We write $F$ and $G$ as Taylor series. Then we expand the cube and rearrange the terms in series, we have:
\begin{equation*}
  F(b) = \sum_{n \geq 0} \frac{F^{(n)}(0)}{n!} \, b^n , \qquad
  G(b)^3 =  \pa{\sum_{n \geq 0} \frac{G^{(n)}(0)}{n!} \, b^n }^3 =  \sum_{n \geq 0} \frac{d_n}{n!} \, b^n .
\end{equation*}
% where $d_n$ is given by
% \begin{equation*}
%   d_n = \sum_{m=0}^n \pvector{n \\ m} \frac{1}{m!} \sum_{k=0}^m \pvector{m \\ k} \pa{ G^{(k)} \cdot G^{(m-k)} \cdot G^{(n-m)}}(0) .
% \end{equation*}
Thus, for every $n \in \N$
\begin{equation*}
  F^{(n)}(0) = d_n .
\end{equation*}
\noindent  Also notice that, because of \cref{prop:measure symmetries}, % [(c)] 
the functions $F$ and $G$ are \emph{even}.
In particular in the above series only even coefficients appear.
  \begin{equation*}
      F(b) = \sum_{n \geq 0} \frac{F^{(2n)}(0)}{(2n)!} \, b^{2n} , \qquad
      G(b)^3 =  \sum_{n \geq 0} \frac{d_{2n}}{(2n)!} \, b^{2n} .
  \end{equation*}
  The first five coefficients of the second expansion are
  \begin{align}
    d_0 &= G(0)^3 \label{eq:1dn} \\
    d_2 &= 3 \, G(0)^2 G^{''}(0) \label{eq:2dn} \\
    d_4 &= \textstyle 3 \, G(0) \pa{ 6 \, G^{''}(0)^2 + G(0) G^{(4)}(0) } \label{eq:3dn}
  \end{align}
  and $d_{2n+1} = 0$ for every $n \geq 0$.

We compute the derivatives:
% \begin{eqnarray*}
%   F^{(2k)}(0) = & (3^{\frac34})^{2k+1} \, \Gamma(\frac54 + \frac k2) \, \mathcal{I}_{2k}, \quad \text{ and } \\ % 2 simplify with the 1/2 in the integral
%   G^{(2k)}(0) = &  2^{-1} \, \Gamma\pa{\frac{5}{12}+\frac k2} .
% \end{eqnarray*}
\begin{align*}
  G^{(2k)}(0) & = \int_{\R} e^{-x^4} (x^2)^{\frac13} x^{2k} \d x \\
             & = 2 \int_0^\infty  e^{-x^4} x^{\frac23 + 2k} \d x \overset{(u=x^4)}{=} \frac12 \int_0^\infty e^{-u} u^{\frac16 + \frac{k}{2}} u^{\frac14 -1} \d u \\
  & = \frac12 \int_0^\infty e^{-u} u^{\frac{5}{12} + \frac{k}{2} -1} \d u = \frac12 \Gamma\pa{\frac{5}{12}+\frac k2}.
\end{align*}

\begin{align*}
  F^{(2k)}(0) & = \int_{\R^2} e^{-\tau} (w\nu*w\nu*w\nu)(\xi,\tau) \xi^{2k} \d \tau \d \xi \qquad (\text{changing variables: } \tau = \lambda \xi^4 ) \\
             & = \int_{\frac{1}{27}}^\infty (w\nu*w\nu*w\nu)(1,\lambda) \bra{2\int_0^\infty e^{-\lambda \xi^4} \xi^{4 + 2k} \d \xi} \d \lambda \\
             & = \frac12 \Gamma\pa{\frac54 + \frac k2} \int_{\frac{1}{27}}^\infty  (w\nu*w\nu*w\nu)(1,\lambda) \frac{\d \lambda}{\lambda^{\frac54 + \frac{k}{2}}} \\
             & = \frac12 \Gamma\pa{\frac54 + \frac k2} \int_0^{3^{\frac{3}{4}}} (w\nu*w\nu*w\nu)(1,t^{-4}) \frac{4\d t}{t^{-2k}} \\
             & = \Gamma\pa{\frac54 + \frac k2} (3^{\frac34})^{2k+1} \int_0^1 (w\nu*w\nu*w\nu)(1,3^{-3}s^{-4}) s^{2k}\d s \\
  & = \Gamma\pa{\frac54 + \frac k2} (3^{\frac34})^{2k+1}  \, \mathcal{I}_{2k} .
\end{align*}
where we changed variables back $\lambda^{-\frac14} = t$, so that $\lambda^{-\frac14 - 1} \d \lambda = -4 \d t$, then we rescaled $t = 3^{\frac34} s$.

The expressions for $F^{(2n)}(0)$ encapsulate the moments $\mathcal{I}_{2n}$.
We compute $\mathcal{I}_{2n}$ comparing the coefficients $F^{(2n)}(0)$ with the explicit values of $d_{2n}$:
\begin{equation}\label{eq:moments_formula}
  \mathcal{I}_{2n} = \frac{d_{2n}}{(3^{\frac34})^{2n+1} \Gamma\pa{\frac54 + \frac n2}}.
\end{equation}

\paragraph{The first coefficient $c_0$} Because of the normalised product we have chosen,
the double of the first coefficient $c_0$ equals the first moment $\mathcal I_0$.
We compare the first coefficients of the two series using the formulas \eqref{eq:moments_formula} and~\cref{eq:1dn,eq:2dn,eq:3dn}.
\begin{equation*}
\begin{cases}
  F(0) & = 3^{\frac34} \Gamma(\frac54) \, 2 \, c_0  \\
  G(0)^3 & = 2^{-3} \pa{\Gamma(\frac{5}{12})}^3
\end{cases}
\quad \then \quad  c_0 = \frac{\pa{\Gamma(\frac{5}{12})}^3}{2^4 3^{\frac34}\Gamma(\frac54)} .
\end{equation*}
  This first coefficient is an old friend. In fact, this quantity is closely related to the constant in front of the squared norm of $g$ in \eqref{eq:quantity_with_coefficients}: that constant is $c_0^{-1}$.
  Truncating the expansion at the first step, what we get is exactly our first approximation \eqref{eq:first_approximation}:
  \begin{equation*}
    \frac{\norm{f \sqrt w \nu * f \sqrt w \nu * f \sqrt w \nu}^2_{L^2(\R^2)} }{\norm{f}^6_{L^2(\R)}} \geq
    \frac{1}{c_0} \cdot c_0^2 = c_0
  \end{equation*}
  Now we can continue computing more coefficients.

  \paragraph{The coefficient $c_2$} To calculate $c_2$ we need the second moment $\mathcal{I}_2$. This is 
\begin{equation*}
  \mathcal{I}_2 = \frac{3 \, G(0)^2 G^{''}(0)}{(3^{\frac34})^3 \, \Gamma(\frac74)} = \frac{3\, \Gamma(\frac{5}{12})^2 \Gamma(\frac{11}{12})}{2^3 3^{\frac94} \, \Gamma(\frac74)} = \alpha_2 \, c_0, \qquad \alpha_2 := \frac{2 \, \Gamma(\frac{11}{12}) \Gamma(\frac14)}{3\sqrt{3} \, \Gamma(\frac{5}{12}) \Gamma(\frac34)} .
\end{equation*}
The second coefficient squared is
\begin{equation*}
  \textstyle  c_2^2 = 5 \nang{g, \frac{1}{2} \, (3 t^2-1) }^2 =
  \frac{5}{4^{\color{blue}2}} ( 3 \mathcal{I}_2 - \mathcal{I}_0 )^2 .
\end{equation*}

% and the sum of the first two coefficients squares is
%  \begin{equation*}
%     c_0^2 + c_2^2 = \pa{ \frac94 - \frac{15}{4}\alpha_2 + \frac{45}{4^2} \alpha^2_2} c_0^2.
%  \end{equation*}
% This gives us the lower bound 
%  \begin{equation*}
%     \frac{\norm{f \sqrt w \nu * f \sqrt w \nu * f \sqrt w \nu}^2_{L^2(\R^2)} }{\norm{f}^6_{L^2(\R)}} \geq
%     \pa{ \frac94 - \frac{15}{4}\alpha_2 + \frac{45}{4^2} \alpha_2^2 } c_0 \approx 0.2997842\dots
%   \end{equation*}
%   unfortunately still not large enough.

  % ------------------------------------- %

  \paragraph{The coefficient $c_4$}  The fourth moment $\mathcal{I}_4$ is:
  \begin{equation*}
    \mathcal{I}_4 =
    \pa{ \frac{2^4}{15} \frac{\Gamma(\frac{11}{12})^2}{ \Gamma(\frac{5}{12})^2} + \frac{2}{3^3} } c_0 = \alpha_4 \, c_0. 
  \end{equation*}
  The fourth coefficient squared is
  \begin{equation*}
    \textstyle  c_4^2 = 9 \nang{g, \frac18 \, (35 t^4 - 30 t^2 +3)}^2 =
    \frac{9}{{\color{blue}4}} \frac{1}{8^2} ( 35 \mathcal{I}_4 - 30 \mathcal{I}_2 + 3 \mathcal{I}_0 )^2 .
  \end{equation*}

  % and the sum of the squares
  % \begin{equation*}
  %   c_0^2 + c_2^2 + c_4^2 = \pa{ \frac94 - \frac{15}{4}\alpha_2 + \frac{45}{4^2} \alpha^2_2} c_0^2.
  % \end{equation*}

  We sum all the coefficients squared that we have calculated so far,
  in terms of $c_0$. After collecting $c_0$, we obtain:
  \begin{equation*}
    c_0^2 + c_2^2 + c_4^2 = \pa{ \frac94 - \frac{15}{4}\alpha + \frac{45}{4^2} \alpha^2 + \frac{9}{2^8} ( 35 \alpha_4 - 30 \alpha_2 + 6 )^2 } c_0^2 .
  \end{equation*}
  Since
  \begin{equation*}
    \frac{c_0^2 + c_2^2 + c_4^2}{c_0} \approx 0.306879 > \frac{\pi}{6 \sqrt{3}}
  \end{equation*}
  this proves a lower bound for $\SharpC$ good enough to ensure that $\SharpC > \Schr$, since
  \begin{equation*}
    \frac{\pi}{3} \SharpC^6 \geq \frac{\norm{f \sqrt w \nu * f \sqrt w \nu * f \sqrt w \nu}^2_{L^2(\R^2)} }{\norm{f}^6_{L^2(\R)}} \geq  \frac{c_0^2 + c_2^2 + c_4^2}{c_0} > \frac{\pi}{3} (\Schr)^6
  \end{equation*}
  This rules out the second case in \cref{th:to_show}, proving existence of extremizers.
\end{proof}  

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../Thesis"
%%% End:
